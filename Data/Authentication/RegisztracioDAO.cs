﻿// <copyright file="RegisztracioDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Authentication
{
    using System;
    using System.Linq;
    using Common;

    /// <summary>
    /// Regisztrálás adatbázis műveletei.
    /// </summary>
    public class RegisztracioDAO
    {
        /// <summary>
        /// Egyedi felhasználónév ellenőrzés
        /// </summary>
        /// <param name="nev">Keresendő felhasználónév</param>
        /// <returns>Létezik nevű user az adatbázisban.</returns>
        public static bool EgyediFelhasznaloNev(string nev)
        {
            AuthDBEntities authDB = new AuthDBEntities();
            return authDB.LOGGING.Where(x => x.FELHASZNALONEV.Equals(nev)).ToList().Count == 0;
        }

        /// <summary>
        /// Új felhasználó rögzítése az AuthDB-be
        /// </summary>
        /// <param name="nev">Felhasználó neve</param>
        /// <param name="jelszo">Jelszó</param>
        /// <param name="jog">Jogosultsága</param>
        /// <param name="teljesNev">Felhasználó teljes neve</param>
        /// <param name="irszam">Irányítószám</param>
        /// <param name="varos">Város</param>
        /// <param name="cim">Szállítási cím</param>
        /// <param name="rendszam">Rendszám (csak futárnál, egyébként bármi lehet)</param>
        /// <param name="korzet">Körzet (csak futárnál, egyébként bármi lehet)</param>
        /// <returns>Sikeres rögzítés</returns>
        public static bool Rogzites(string nev, string jelszo, string jog, string teljesNev, string irszam, string varos, string cim, string rendszam, decimal? korzet)
        {
            AuthDBEntities authDB = new AuthDBEntities();
            LOGGING logging = new LOGGING();
            logging.ID = nev.GetHashCode();
            logging.FELHASZNALONEV = nev;
            logging.JELSZO = jelszo;
            logging.JOGOSULTSAG = jog;

            CsomagAllDBEntities csomagAllDB = new CsomagAllDBEntities();
            if (string.Compare(Consts.Megrendelo, jog, true) == 0)
            {
                // ha kész vannak a regisztrációs felületek, akkor azokhoz mérten ezt is át kell írni
                MEGRENDELO m = new MEGRENDELO();
                m.ID = nev.GetHashCode();
                m.Nev = teljesNev;
                m.Varos = varos;
                m.Cim = cim;
                m.Iranyitoszam = irszam;
                logging.LOGGEDUSERID = m.ID;
                csomagAllDB.MEGRENDELO.Add(m);
            }
            else if (string.Compare(Consts.Futar, jog, true) == 0)
            {
                FUTAR f = new FUTAR();
                f.Elerhetoseg = nev + "@csomagall.com";
                f.ID = nev.GetHashCode();
                f.Korzet = korzet;
                f.Nev = teljesNev;
                f.Rendszam = rendszam;
                logging.LOGGEDUSERID = f.ID;
                csomagAllDB.FUTAR.Add(f);
            }
            else if (string.Compare(Consts.Vezeto, jog, true) == 0)
            {
                RAKTARVEZETO rv = new RAKTARVEZETO();
                rv.ID = nev.GetHashCode();
                rv.Korzet = 10;
                rv.Nev = teljesNev;
                logging.LOGGEDUSERID = rv.ID;
                csomagAllDB.RAKTARVEZETO.Add(rv);
            }
            else
            {
                throw new Exception("Nem létező jogosultság!");
            }

            authDB.LOGGING.Add(logging);
            authDB.SaveChanges();
            csomagAllDB.SaveChanges();
            return true;
        }
    }
}
