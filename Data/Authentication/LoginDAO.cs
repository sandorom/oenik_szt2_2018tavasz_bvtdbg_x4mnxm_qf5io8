﻿// <copyright file="LoginDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Authentication
{
    using System;
    using System.Linq;

    /// <summary>
    /// Bejelentkezés műveletei
    /// </summary>
    public class LoginDAO : IDisposable
    {
        /// <summary>
        /// AuthDB referencia
        /// </summary>
        private AuthDBEntities auth = new AuthDBEntities();

        /// <summary>
        /// CsomagAllDB referencia
        /// </summary>
        private CsomagAllDBEntities csomagAll = new CsomagAllDBEntities();

        /// <summary>
        /// Felhasználót keres usernév és jelszó alapján. Ha talál, akkor visszaadja az objektumot, egyéb esetben excpetion.
        /// </summary>
        /// <param name="loginNev">Titkosított bejelentkezési név</param>
        /// <param name="jelszo">Titkosított jelszó</param>
        /// <returns>Talált objektum / exception</returns>
        public LOGGING SikeresBejelentkezes(string loginNev, string jelszo)
        {
            // Exception dobódik ha nincs ilyen felhasználó-jelszó páros
            return this.auth.LOGGING.Single(x => x.FELHASZNALONEV.Equals(loginNev) && x.JELSZO.Equals(jelszo));
        }

        /// <summary>
        /// ID alapján megkeresi a megrendelőt.
        /// </summary>
        /// <param name="id">Megrendelő ID-ja</param>
        /// <returns>Visszaadja a megrendelőt</returns>
        public MEGRENDELO MegrendeloIdAlapjan(decimal id)
        {
            return this.csomagAll.MEGRENDELO.Single(x => x.ID == id);
        }

        /// <summary>
        /// ID alapján megkeresi a futárt.
        /// </summary>
        /// <param name="id">Futár ID-ja</param>
        /// <returns>Visszaadja a futárt</returns>
        public FUTAR FutarIdAlapjan(decimal id)
        {
            return this.csomagAll.FUTAR.Single(x => x.ID == id);
        }

        /// <summary>
        /// ID alapján megkeresi a raktárvezetőt.
        /// </summary>
        /// <param name="id">Raktárvezető ID-ja</param>
        /// <returns>Visszaadja a raktárvezetőt</returns>
        public RAKTARVEZETO RaktarVezetoIdAlapjan(decimal id)
        {
            return this.csomagAll.RAKTARVEZETO.Single(x => x.ID == id);
        }

        /// <summary>
        /// Auto-generated (???)
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)this.auth).Dispose();
            ((IDisposable)this.csomagAll).Dispose();
        }

        /// <summary>
        /// Adatbázis előtöltésre. Nem használt metódus, csak az első bejelentkezés lassúságát kerüljük meg a Count hívással.
        /// </summary>
        public void PreLoad()
        {
            this.auth.LOGGING.Count();
            this.csomagAll.TERMEK.Count();
        }
    }
}
