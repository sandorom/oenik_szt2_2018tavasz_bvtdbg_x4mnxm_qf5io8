﻿// <copyright file="IFutarKezeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    /// <summary>
    /// Futár-kezelés előírások
    /// </summary>
    public interface IFutarKezeles
    {
        /// <summary>
        /// Futár elérhetőségének frissítése
        /// </summary>
        /// <param name="futar">Frissíteni kívánt futár</param>
        void ElerhetosegFrissites(FUTAR futar);

        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        void AllapotFrissites(SZALLITAS ujSzallitas);
    }
}
