﻿// <copyright file="IUzenetkezeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    using System.Collections.Generic;
    using Common;

    /// <summary>
    /// Előírja milyen funkciókkal kell rendelkeznie azon felhasználóknak, akik részt vesznek üzenet küldésben.
    /// </summary>
    public interface IUzenetkezeles
    {
        /// <summary>
        /// Rögzít egy új üzenetet.
        /// </summary>
        /// <param name="uzenet">Felparaméterezett üzenet objektum</param>
        void UjUzenet(UZENET uzenet);

        /// <summary>
        /// Felhasználó üzenetei
        /// </summary>
        /// <param name="id">Felhasználó saját azonosítója</param>
        /// <returns>Visszaadja a felhasználó összes üzenetét</returns>
        List<UzenetForGui> FelhasznaloUzenetei(decimal id);
    }
}
