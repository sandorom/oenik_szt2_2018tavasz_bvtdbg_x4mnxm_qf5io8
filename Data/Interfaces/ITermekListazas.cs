﻿// <copyright file="ITermekListazas.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Termékek listázásához szükséges metódusok.
    /// </summary>
    public interface ITermekListazas
    {
        /// <summary>
        /// Összes raktáron lévő termék listázása.
        /// </summary>
        /// <returns>Összes raktáron lévő termék listázása</returns>
        List<TERMEK> ElerhetoTermekek();

        /// <summary>
        /// Elérhető termékek adott raktárban.
        /// </summary>
        /// <param name="raktar">Válaszott raktár</param>
        /// <returns>Adott raktár összes elérhető terméke.</returns>
        List<TERMEK> ElerhetoTermekekRaktarSzerint(RAKTAR raktar);
    }
}
