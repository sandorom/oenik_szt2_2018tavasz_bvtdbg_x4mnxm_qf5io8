﻿// <copyright file="ISzallitasKezeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Szállításkezeléssel kapcsolatos előírások
    /// </summary>
    public interface ISzallitasKezeles
    {
        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        void AllapotFrissites(SZALLITAS ujSzallitas);

        /// <summary>
        /// Szállítás objektumok közti szűrés.
        /// Ha valamire nem szeretnénk szűrni, akkor null értéket kell megadni!
        /// (string esetén: string.Empty)
        /// </summary>
        /// <param name="termekID">Szűréshez termék azonosító</param>
        /// <param name="raktarID">Szűréshez raktár azonosító</param>
        /// <param name="futarID">Szűréshez futár azonosító</param>
        /// <param name="allapot">Szállítás aktuális állapota</param>
        /// <param name="mennyiseg">Rendelt mennyiség</param>
        /// <returns>Szűrt szállítás lista</returns>
        List<SZALLITAS> SzallitasSzures(int? termekID, int? raktarID, int? futarID, string allapot, int? mennyiseg);
    }
}
