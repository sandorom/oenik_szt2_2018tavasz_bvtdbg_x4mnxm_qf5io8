﻿// <copyright file="Consts.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Common
{
    /// <summary>
    /// Az adatbázisokhoz szükséges konstansok
    /// </summary>
    internal class Consts
    {
        /// <summary>
        /// Raktárvezető felhaszáló jogosultsága
        /// </summary>
        public const string Vezeto = "vezeto";

        /// <summary>
        /// Futár felhaszáló jogosultsága
        /// </summary>
        public const string Futar = "futar";

        /// <summary>
        /// Megrendelő felhaszáló jogosultsága
        /// </summary>
        public const string Megrendelo = "megrendelo";

        /*****/

        // SZÁLLÍTÁS ÁLLAPOTOK

        /// <summary>
        /// Megrendelt állapot
        /// </summary>
        public const string SzallitasMegrendelve = "Megrendelve";

        /// <summary>
        /// Rögzitett állapot
        /// </summary>
        public const string SzallitasRogzitve = "Rogzitve";

        /// <summary>
        /// Kiszállítva
        /// </summary>
        public const string SzallitasKiszallitva = "Kiszallitva";

        /// <summary>
        /// Kiszállítás alatt
        /// </summary>
        public const string SzallitasKiszallitasAlatt = "KiszallitasAlatt";

        /// <summary>
        /// Törölt állapot
        /// </summary>
        public const string SzallitasTorolve = "Torolve";

        /// <summary>
        /// Sikertelen kézbesítés
        /// </summary>
        public const string SikerteleKezbesites = "SikertelenKezbesites";

        /*****/

        // FUTÁR ÁLLAPOTOK

        /// <summary>
        /// Futár elérhető
        /// </summary>
        public const string FutarElerheto = "Elerheto";

        /// <summary>
        /// Futár túlterhelt
        /// </summary>
        public const string FutarTulterhelt = "Tulterhelt";

        /// <summary>
        /// Futár beteg
        /// </summary>
        public const string FutarBeteg = "Beteg";
    }
}
