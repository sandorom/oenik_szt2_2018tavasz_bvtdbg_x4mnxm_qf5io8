﻿// <copyright file="UzenetForGui.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.Common
{
    /// <summary>
    /// Az adatbázisban tárolt üzeneteket nem tudjuk egy az egyben megjeleníteni a felületeken, ezért szükséges ez a "konvertáló" objektum.
    /// </summary>
    public class UzenetForGui : UZENET
    {
        /// <summary>
        /// Gets or sets feladó neve
        /// </summary>
        public string Felado { get; set; }

        /// <summary>
        /// Gets or sets üzenet tárgya
        /// </summary>
        public new string Targy { get; set; }

        /// <summary>
        /// Gets or sets üzenet tartalma
        /// </summary>
        public string Szoveg { get; set; }

        /// <summary>
        /// Gets or sets teljes üzenet objektum
        /// </summary>
        public UZENET Uzenet { get; set; }

        /// <summary>
        /// Létrehoz és felparaméterez egy üzenet objektumot, ami csak a felületen való megjelenéshez használható
        /// </summary>
        /// <param name="felado">Feladó neve</param>
        /// <param name="targy">Üzenet tárgya</param>
        /// <param name="szoveg">Üzenet tartalma</param>
        /// <param name="uzenet">Teljes üzenet objektum</param>
        /// <returns>Felparaméterezett UzenetForGui objektum</returns>
        public static UzenetForGui UzenetForGuiBuilder(string felado, string targy, string szoveg, UZENET uzenet)
        {
            return new UzenetForGui()
            {
                Felado = felado,
                Targy = targy,
                Szoveg = szoveg,
                Uzenet = uzenet
            };
        }
    }
}
