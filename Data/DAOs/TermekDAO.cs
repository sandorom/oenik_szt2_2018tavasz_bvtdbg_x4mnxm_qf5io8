﻿// <copyright file="TermekDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.DAOs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data.Interfaces;

    /// <summary>
    /// Termékek adatbázis műveletei.
    /// </summary>
    public class TermekDAO : ITermekListazas, IDisposable
    {
        /// <summary>
        /// CsomagAllDB referencia.
        /// </summary>
        private CsomagAllDBEntities csomagAllDB = new CsomagAllDBEntities();

        /// <summary>
        /// Elérhető termékek listázása.
        /// </summary>
        /// <returns>Összes raktár összes terméke.</returns>
        public List<TERMEK> ElerhetoTermekek()
        {
            var elerheto = from t in this.csomagAllDB.TERMEK
                           join k in this.csomagAllDB.KESZLET on t.ID equals k.Termek_ID
                           where k.Keszlet_Mennyiseg > 0
                           select new
                            {
                                t.ID,
                                t.Ar,
                                t.Leiras,
                                t.Nev,
                                t.Suly
                            };

            List<TERMEK> result = new List<TERMEK>();
            foreach (var item in elerheto)
            {
                result.Add(new TERMEK() { Ar = item.Ar, ID = item.ID, Leiras = item.Leiras, Nev = item.Nev, Suly = item.Suly });
            }

            return result;
        }

        /// <summary>
        /// Adott raktár elérhető termékei.
        /// </summary>
        /// <param name="raktar">Válaszott raktár.</param>
        /// <returns>Válaszott raktár készleten lévő termékei.</returns>
        public List<TERMEK> ElerhetoTermekekRaktarSzerint(RAKTAR raktar)
        {
            var elerheto = from t in this.csomagAllDB.TERMEK
                                           join k in this.csomagAllDB.KESZLET on t.ID equals k.Termek_ID
                                           join r in this.csomagAllDB.RAKTAR on k.Raktar_ID equals r.ID
                                           where k.Keszlet_Mennyiseg > 0 && r.ID == raktar.ID
                                           select new
                                           {
                                               t.ID,
                                               t.Ar,
                                               t.Leiras,
                                               t.Nev,
                                               t.Suly
                                           };

            List<TERMEK> result = new List<TERMEK>();
            foreach (var item in elerheto)
            {
                result.Add(new TERMEK() { Ar = item.Ar, ID = item.ID, Leiras = item.Leiras, Nev = item.Nev, Suly = item.Suly });
            }

            return result;
        }

        /// <summary>
        /// Új termék rögzítése az adatbázisba.
        /// </summary>
        /// <param name="termek">Új termék objektum</param>
        public void UjTermekMentes(TERMEK termek)
        {
            this.csomagAllDB.TERMEK.Add(termek);
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Meglévő termék módosítása.
        /// </summary>
        /// <param name="modositottTermek">Módosítitott adatokat tartalmazó objektum</param>
        public void TermekModositas(TERMEK modositottTermek)
        {
            TERMEK regiTermek = this.csomagAllDB.TERMEK.Single(x => x.ID == modositottTermek.ID);
            regiTermek.Ar = modositottTermek.Ar;
            regiTermek.Leiras = modositottTermek.Leiras;
            regiTermek.Suly = modositottTermek.Suly;
            regiTermek.Nev = modositottTermek.Nev;
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Adott termék adott raktárban lévő mennyisége.
        /// </summary>
        /// <param name="raktarId">Választott raktár azonosító</param>
        /// <param name="termekId">Keresett termék azonosító</param>
        /// <returns>Raktáron lévő termék mennyisége</returns>
        public int KeszletenLevoTermekRaktarban(int raktarId, int termekId)
        {
            KESZLET keszleten = this.csomagAllDB.KESZLET.Single(x => x.Raktar_ID == raktarId && x.Termek_ID == termekId);
            return decimal.ToInt32((decimal)keszleten.Keszlet_Mennyiseg);
        }

        /// <summary>
        /// Vásárláskor készlet csökkentés.
        /// </summary>
        /// <param name="raktarId">Raktár azonosító</param>
        /// <param name="termekId">Vásárolt termék azonosítója</param>
        /// <param name="mennyiseg">Vásárolt termék mennyisége</param>
        public void KeszletCsokkentes(int raktarId, int termekId, int mennyiseg)
        {
            KESZLET keszlet = this.csomagAllDB.KESZLET.Single(x => x.Raktar_ID == raktarId && x.Termek_ID == termekId);
            keszlet.Keszlet_Mennyiseg -= mennyiseg;
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Termék módosításkor készlet növelés.
        /// </summary>
        /// <param name="raktarId">Raktár azonosító</param>
        /// <param name="termekId">Termék azonosítója</param>
        /// <param name="mennyiseg">Termék új mennyisége</param>
        public void KeszletModositas(int raktarId, int termekId, int mennyiseg)
        {
            try
            {
                KESZLET keszlet = this.csomagAllDB.KESZLET.Single(x => x.Raktar_ID == raktarId && x.Termek_ID == termekId);
                keszlet.Keszlet_Mennyiseg = mennyiseg;
            }
            catch (InvalidOperationException)
            {
                this.csomagAllDB.KESZLET.Add(new KESZLET() { ID = termekId - raktarId - mennyiseg, Keszlet_Mennyiseg = mennyiseg, Raktar_ID = raktarId, Termek_ID = termekId });
            }

            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Auto-generated (???)
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)this.csomagAllDB).Dispose();
        }
    }
}
