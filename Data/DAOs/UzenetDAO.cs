﻿// <copyright file="UzenetDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.DAOs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Interfaces;

    /// <summary>
    /// Üzenet adatbázis-műveletek
    /// </summary>
    public class UzenetDAO : IUzenetkezeles, IDisposable
    {
        /// <summary>
        /// CsomagAllDB referencia
        /// </summary>
        private CsomagAllDBEntities csomagAllDB = new CsomagAllDBEntities();

        /// <summary>
        /// Felhasználó üzenetei
        /// </summary>
        /// <param name="id">Felhasználó saját azonosítója</param>
        /// <returns>Visszaadja a felhasználó összes üzenetét dátum szerint rendezve</returns>
        public List<UzenetForGui> FelhasznaloUzenetei(decimal id)
        {
            List<UzenetForGui> uzenetek = new List<UzenetForGui>();
            foreach (UZENET item in this.csomagAllDB.UZENET.Where(x => x.Cimzett_ID == id || x.Kuldo_ID == id).OrderByDescending(x => x.Beerkezett).ToList())
            {
                string felado = this.NevById(item.Kuldo_ID);
                uzenetek.Add(UzenetForGui.UzenetForGuiBuilder(felado, item.Targy, item.Tartalom, item));
            }

            return uzenetek;
        }

        /// <summary>
        /// Rögzít egy új üzenetet.
        /// </summary>
        /// <param name="uzenet">Felparaméterezett üzenet objektum</param>
        public void UjUzenet(UZENET uzenet)
        {
            this.csomagAllDB.UZENET.Add(uzenet);
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// A megrendelők ezeknek a raktárvezetőknek küldhetnek üzenetet.
        /// </summary>
        /// <returns>Összes raktárvezető</returns>
        public List<RAKTARVEZETO> RaktarVezetok()
        {
            return this.csomagAllDB.RAKTARVEZETO.ToList();
        }

        /// <summary>
        /// Visszaadja a nevet id alapján. Raktárvezetőre és megrendelőre is használható
        /// </summary>
        /// <param name="id">Raktárvezető / megrendelő azonosító</param>
        /// <returns>Felhasználó neve</returns>
        public string NevById(decimal id)
        {
            try
            {
                return this.csomagAllDB.RAKTARVEZETO.Single(x => x.ID == id).Nev;
            }
            catch (InvalidOperationException)
            {
                return this.csomagAllDB.MEGRENDELO.Single(x => x.ID == id).Nev;
            }
        }

        /// <summary>
        /// Auto-generated (???)
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)this.csomagAllDB).Dispose();
        }
    }
}
