﻿// <copyright file="FutarDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.DAOs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interfaces;

    /// <summary>
    /// Futár adatbázis-műveletek
    /// </summary>
    public class FutarDAO : IFutarKezeles, IDisposable
    {
        /// <summary>
        /// CsomagAllDB referencia
        /// </summary>
        private CsomagAllDBEntities csomagAllDB = new CsomagAllDBEntities();

        /// <summary>
        /// Szállítással kapcsolatos adatbázis-műveletek
        /// </summary>
        private SzallitasDAO szallitasDAO = new SzallitasDAO();

        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        public void AllapotFrissites(SZALLITAS ujSzallitas)
        {
            this.szallitasDAO.AllapotFrissites(ujSzallitas);
        }

        /// <summary>
        /// Futár elérhetőségének frissítése
        /// </summary>
        /// <param name="futar">Frissíteni kívánt futár</param>
        public void ElerhetosegFrissites(FUTAR futar)
        {
            FUTAR fut = this.csomagAllDB.FUTAR.Single(x => x.ID == futar.ID);
            fut.Elerhetoseg = futar.Elerhetoseg;
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Futár listázás raktár szerint
        /// </summary>
        /// <param name="raktar">Raktár, amire szűrni kell</param>
        /// <returns>Raktár összes futára</returns>
        public List<FUTAR> FutarokRaktarSzerint(RAKTAR raktar)
        {
            return this.csomagAllDB.FUTAR.Where(x => x.Korzet == raktar.Korzet).ToList();
        }

        /// <summary>
        /// Auto-generated (???)
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)this.csomagAllDB).Dispose();
        }
    }
}
