﻿// <copyright file="SzallitasDAO.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Data.DAOs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Interfaces;

    /// <summary>
    /// Szállítás adatbázis-műveletek
    /// </summary>
    public class SzallitasDAO : ISzallitasKezeles, IDisposable
    {
        /// <summary>
        /// CsomagAllDB referencia
        /// </summary>
        private CsomagAllDBEntities csomagAllDB = new CsomagAllDBEntities();

        /// <summary>
        /// Szállítás állapot frissítés (a tesztek miatt így lehetett csak megoldani)
        /// </summary>
        /// <param name="ujSzallitas">Új állapotú szállítás</param>
        /// <param name="regiSzallitas">Regi objektum</param>
        public static void SzallitasFrissites(SZALLITAS ujSzallitas, ref SZALLITAS regiSzallitas)
        {
            regiSzallitas.Allapot = ujSzallitas.Allapot;
            if (Consts.SzallitasKiszallitva.Equals(ujSzallitas.Allapot) || Consts.SikerteleKezbesites.Equals(ujSzallitas.Allapot))
            {
                regiSzallitas.Szallitas_vege = DateTime.Now;
            }
        }

        /// <summary>
        /// Egy új szállítás objektumot rögzít a DB-be
        /// </summary>
        /// <param name="szallitas">Felparaméterezett szállítás objektum</param>
        public void Rogzites(SZALLITAS szallitas)
        {
            this.csomagAllDB.SZALLITAS.Add(szallitas);
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Adott raktár szállításai
        /// </summary>
        /// <param name="raktarID">Tárgyalt raktár azonosítója</param>
        /// <returns>Visszaadja az adott raktár összes szállítását</returns>
        public List<SZALLITAS> RaktarhozTartozoSzallitasok(int raktarID)
        {
            return this.csomagAllDB.SZALLITAS.Where(x => x.Raktar_ID == raktarID).ToList();
        }

        /// <summary>
        /// Adott raktár aktív szállításai
        /// </summary>
        /// <param name="raktarID">Tárgyalt raktár azonosítója</param>
        /// <returns>Visszaadja az adott raktár összes AKTÍV szállítását</returns>
        public List<SZALLITAS> RaktarhozTartozoAktivSzallitasok(decimal raktarID)
        {
            return this.csomagAllDB.SZALLITAS.Where(x => x.Raktar_ID == raktarID && (
                                                                                        x.Allapot == Consts.SzallitasKiszallitasAlatt
                                                                                     || x.Allapot == Consts.SzallitasMegrendelve)).ToList();
        }

        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        public void AllapotFrissites(SZALLITAS ujSzallitas)
        {
            SZALLITAS regiSzallitas = this.csomagAllDB.SZALLITAS.Single(x => x.ID == ujSzallitas.ID);
            SzallitasFrissites(ujSzallitas, ref regiSzallitas);
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Szállítás objektumok közti szűrés.
        /// Ha valamire nem szeretnénk szűrni, akkor null értéket kell megadni!
        /// (string esetén: string.Empty)
        /// </summary>
        /// <param name="termekID">Szűréshez termék azonosító</param>
        /// <param name="raktarID">Szűréshez raktár azonosító</param>
        /// <param name="futarID">Szűréshez futár azonosító</param>
        /// <param name="allapot">Szállítás aktuális állapota</param>
        /// <param name="mennyiseg">Rendelt mennyiség</param>
        /// <returns>Szűrt szállítás lista</returns>
        public List<SZALLITAS> SzallitasSzures(int? termekID, int? raktarID, int? futarID, string allapot, int? mennyiseg)
        {
            IEnumerable<SZALLITAS> szallitas = this.csomagAllDB.SZALLITAS;
            if (termekID != null)
            {
                szallitas = szallitas.Where(x => x.Termek_ID == termekID);
            }

            if (raktarID != null)
            {
                szallitas = szallitas.Where(x => x.Raktar_ID == raktarID);
            }

            if (futarID != null)
            {
                szallitas = szallitas.Where(x => x.Futar_ID == futarID);
            }

            if (!string.Empty.Equals(allapot))
            {
                szallitas = szallitas.Where(x => x.Allapot == allapot);
            }

            if (mennyiseg != null)
            {
                szallitas = szallitas.Where(x => x.Mennyiseg == mennyiseg);
            }

            return szallitas.ToList();
        }

        /// <summary>
        /// Megrendelő rendelései
        /// </summary>
        /// <param name="id">Megrendelő azonosítója</param>
        /// <returns>A megrendelő nem törölt állapotú rendelései</returns>
        public List<SZALLITAS> MegrendeloRendelesei(decimal? id)
        {
            return this.csomagAllDB.SZALLITAS.Where(x => x.Megrendelo_ID == id).OrderByDescending(x => x.Szallitas_kezdete).ToList();
        }

        /// <summary>
        /// Futárhoz szállítás rendelés
        /// </summary>
        /// <param name="szallitas">Szállítandó termékek</param>
        /// <param name="futar">Kiszállítást végző futár</param>
        public void FutarhozRendeles(SZALLITAS szallitas, FUTAR futar)
        {
            SZALLITAS sz = this.csomagAllDB.SZALLITAS.Single(x => x.ID == szallitas.ID);
            sz.Futar_ID = futar.ID;
            sz.Szallitas_kezdete = DateTime.Now;
            sz.Allapot = Consts.SzallitasKiszallitasAlatt;
            this.csomagAllDB.SaveChanges();
        }

        /// <summary>
        /// Raktár ID alapján
        /// </summary>
        /// <param name="raktarKorzet">Keresett raktár ID-ja</param>
        /// <returns>Raktár</returns>
        public RAKTAR RaktarByKorzet(decimal raktarKorzet)
        {
            return this.csomagAllDB.RAKTAR.Single(x => x.Korzet == raktarKorzet);
        }

        /// <summary>
        /// Auto-generated (???)
        /// </summary>
        public void Dispose()
        {
            ((IDisposable)this.csomagAllDB).Dispose();
        }
    }
}
