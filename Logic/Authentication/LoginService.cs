﻿// <copyright file="LoginService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Authentication
{
    using System;
    using Common;
    using Data;
    using Data.Authentication;
    using Enums;

    /// <summary>
    /// Bejelentkezési műveletek
    /// </summary>
    public static class LoginService
    {
        /// <summary>
        /// Bejelentkezési folyamatok kezelése. Jogosultság visszaadása, jelzés hiba esetén
        /// </summary>
        /// <param name="loginNev">Bejelentkezési név</param>
        /// <param name="jelszo">Jelszó</param>
        /// <returns>Bejelentkezett felhasználó jogosultsága. Sikertelen login esetén CsomagAllException.</returns>
        public static object[] Bejelentkezes(string loginNev, string jelszo)
        {
            LoginDAO loginDAO = new LoginDAO();
            string encLoginNev = TitkositasService.Encrypt(loginNev);
            string encJelszo = TitkositasService.Encrypt(jelszo);
            try
            {
                LOGGING result = loginDAO.SikeresBejelentkezes(encLoginNev, encJelszo);
                string jog = result.JOGOSULTSAG;
                if (string.Compare(JogosultsagEnum.Megrendelo.ToString(), jog, true) == 0)
                {
                    MEGRENDELO user = loginDAO.MegrendeloIdAlapjan(result.LOGGEDUSERID.Value);
                    return new object[] { result.JOGOSULTSAG, user };
                }

                if (string.Compare(JogosultsagEnum.Vezeto.ToString(), jog, true) == 0)
                {
                    RAKTARVEZETO user = loginDAO.RaktarVezetoIdAlapjan(result.LOGGEDUSERID.Value);
                    return new object[] { result.JOGOSULTSAG, user };
                }

                if (string.Compare(JogosultsagEnum.Futar.ToString(), jog, true) == 0)
                {
                    FUTAR user = loginDAO.FutarIdAlapjan(result.LOGGEDUSERID.Value);
                    return new object[] { result.JOGOSULTSAG, user };
                }
            }
            catch (Exception)
            {
                // azért catch-eljük, mert a Single függvény exceptiont dob, ha nincs ilyen felhasználó-jelszó páros
                throw new CsomagAllException(CsomagAllExceptionTipus.Sikertelen_Bejelentkezes, "Helytelen felhasználónév vagy jelszó! Kérjük próbálja újra!");
            }

            // nem létező joggal próbálnak belépni
            throw new CsomagAllException(CsomagAllExceptionTipus.Sikertelen_Bejelentkezes, "Hiba lépett fel a jogosultság-ellenőrzés során!");
        }
    }
}
