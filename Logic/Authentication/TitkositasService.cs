﻿// <copyright file="TitkositasService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Authentication
{
    using System;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using Common;

    /// <summary>
    /// Felhasználónevek és jelszavak hash-eléséhez. Csak titkosítás után menthető adat az AuthDB-be!
    /// </summary>
    public class TitkositasService
    {
        // jelszó titkosításhoz szükséges stringek
        private const string PasswordHash = "Cs0m4gAllP@@Ssw0rd";
        private const string SaltKey = "S@LT&k3Y";
        private const string VIKey = "@1B2c3D4e5F6g7H8";

        /// <summary>
        /// Jelszó titkosítását végzi.
        /// </summary>
        /// <param name="stringToEncrypt">Titkosítandó szöveg</param>
        /// <returns>Titkosított szöveg</returns>
        public static string Encrypt(string stringToEncrypt)
        {
            if (string.IsNullOrEmpty(stringToEncrypt))
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Ures_Felhasznalonev_Jelszo, "Érvénytelen felhasználónév/jelszó!");
            }

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(stringToEncrypt);

            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }

                memoryStream.Close();
            }

            return Convert.ToBase64String(cipherTextBytes);
        }

        /// <summary>
        /// Titkosított szöveg visszafejtése.
        /// </summary>
        /// <param name="encryptedText">Titkosított szöveg</param>
        /// <returns>Visszafejtett szöveg</returns>
        public static string Decrypt(string encryptedText)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encryptedText);
            byte[] keyBytes = new Rfc2898DeriveBytes(PasswordHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}
