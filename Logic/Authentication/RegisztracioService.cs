﻿// <copyright file="RegisztracioService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Authentication
{
    using System;
    using System.Text.RegularExpressions;
    using Common;
    using Data.Authentication;
    using Enums;

    /// <summary>
    /// Regisztrálást végző osztály
    /// </summary>
    public class RegisztracioService
    {
        /// <summary>
        /// Felhasználónév egyediség ellenőrzés, regex validálás, titkosítás és mentés.
        /// </summary>
        /// <param name="teljesNev">Felhasználó teljes neve</param>
        /// <param name="nev">Új felhasználó neve</param>
        /// <param name="jelszo">Jelszó</param>
        /// <param name="jogosultsag">Új felhasználó jogosultsága (pozicíója)</param>
        /// <param name="irszam">Irányítószám</param>
        /// <param name="varos">Város</param>
        /// <param name="cim">Szállítási cím</param>
        /// <param name="rendszam">Rendszám (csak futárnál, egyébként bármi lehet)</param>
        /// <param name="korzet">Körzet (csak futárnál, egyébként bármi lehet)</param>
        /// <returns>Sikeres regisztráció</returns>
        public bool Regisztralas(string teljesNev, string nev, string jelszo, string jogosultsag, string irszam, string varos, string cim, string rendszam, decimal? korzet)
        {
            // Ellenőrizzük, hogy valid jogosultságot kaptunk-e
            if (string.Compare(JogosultsagEnum.Vezeto.ToString(), jogosultsag, true) == 0 || string.Compare(JogosultsagEnum.Futar.ToString(), jogosultsag, true) == 0 || string.Compare(JogosultsagEnum.Megrendelo.ToString(), jogosultsag, true) == 0)
            {
                // nullcheck
                if (string.IsNullOrEmpty(nev) || string.IsNullOrEmpty(jelszo))
                {
                    throw new CsomagAllException(CsomagAllExceptionTipus.Ures_Felhasznalonev_Jelszo, "Adjon meg felhasználónevet és jelszót!");
                }
                else
                {
                    // az ellenőrzések miatt, muszáj már itt titkosítani a felhasználónevet, mert így vannak eltárolva a DB-be
                    string nevEncrypted = TitkositasService.Encrypt(nev);
                    if (this.FelhasznaloNevEsJelszoValidalas(nev, nevEncrypted, jelszo))
                    {
                        // ha minden ellenőrzésen átmentünk, titkosítás és rögzítés
                        string jelszoEncrypted = TitkositasService.Encrypt(jelszo);
                        try
                        {
                            return RegisztracioDAO.Rogzites(nevEncrypted, jelszoEncrypted, jogosultsag, teljesNev, irszam, varos, cim, rendszam, korzet);
                        }
                        catch (Exception ex)
                        {
                            throw new CsomagAllException(CsomagAllExceptionTipus.Inkonzisztens_Allapot, "Adatbázis hiba lépett fel: " + ex.Message);
                        }
                    }
                }
            }

            // nem valid jogosultság
            throw new CsomagAllException(CsomagAllExceptionTipus.Inkonzisztens_Allapot, "Nem megfelelő jogosultságot adott meg!");
        }

        /// <summary>
        /// Validálja a felhasználónevet és jelszó párost. Szüksége van a titkosított felhasználónévre is
        /// </summary>
        /// <param name="nev">Felhasználónév</param>
        /// <param name="nevEncrypted">Titkosított felhasználónév</param>
        /// <param name="jelszo">Jelszó</param>
        /// <returns>Validálás sikeressége</returns>
        private bool FelhasznaloNevEsJelszoValidalas(string nev, string nevEncrypted, string jelszo)
        {
            Regex usernameRegex = new Regex(Consts.FelhasznalonevRegexp);
            Regex passwordRegex = new Regex(Consts.JelszoRegexp);

            // egyediség-vizsgálat, ha nem egyedi, jelezzük
            if (!RegisztracioDAO.EgyediFelhasznaloNev(nevEncrypted))
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Letezo_Username, "Ilyen nevű felhasználó már létezik. Kérjük válasszon más nevet!");
            }

            // ha nem matchel a usernév vagy a jelszó jelezzük
            if (!usernameRegex.IsMatch(nev) && nev.Length > 4)
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Inkonzisztens_Allapot, "A felhasználónévben csak betűk, számok és aláhúzás jelek (\"_\") szerepelhetnek, de nem tartalmazhat ékezetes betűket ÉS legalább 5 karakter hosszúnak kell lennie!");
            }

            if (!passwordRegex.IsMatch(jelszo) && nev.Length > 4)
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Inkonzisztens_Allapot, "A jelszónak legalább 5 karakter hosszúnak kell lennie, valamint csak a következő speciális karaktereket tartalmazhatja: $ # & @ . : , ? ; ! % = * - _ < > |");
            }

            // minden ellenőrzésen átment (minden egyéb esetben exception)
            return true;
        }
    }
}
