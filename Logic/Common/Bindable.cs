﻿// <copyright file="Bindable.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Common
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;

    /// <summary>
    /// Ha változott egy tulajdonság értéke, akkor ez az osztály értesítí a GUI-t, hogy megjelenjen a felületen
    /// </summary>
    public class Bindable : INotifyPropertyChanged
    {
        /// <summary>
        /// A érték megváltozásának eseménye
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Eseméyt elsütő metódus
        /// </summary>
        /// <param name="s">.</param>
        protected void Opc([CallerMemberName] string s = "")
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(s));
            }
        }
    }
}
