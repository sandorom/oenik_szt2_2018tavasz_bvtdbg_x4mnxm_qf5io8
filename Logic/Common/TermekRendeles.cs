﻿// <copyright file="TermekRendeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.ViewModels
{
    using Data;
    using Logic.Common;

    /// <summary>
    /// Csak a MegrendeloVM-ben használt objektum! Nem kerül rögzítésre a DB-ben.
    /// </summary>
    public class TermekRendeles : Bindable
    {
        /// <summary>
        /// megrendelni kívánt termék mennyisége
        /// </summary>
        private int mennyiseg;

        /// <summary>
        /// Gets or sets megrendelni kívánt termék
        /// </summary>
        public TERMEK Termek { get; set; }

        /// <summary>
        /// Gets or sets választott raktár ID-ja
        /// </summary>
        public int Raktar_Id { get; set; }

        /// <summary>
        /// Gets or sets mennyiség
        /// </summary>
        public int Mennyiseg
        {
            get
            {
                return this.mennyiseg;
            }

            set
            {
                this.mennyiseg = value;
                this.Opc();
            }
        }
    }
}
