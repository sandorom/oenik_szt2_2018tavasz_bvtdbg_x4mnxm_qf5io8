﻿// <copyright file="Consts.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Common
{
    /// <summary>
    /// A projekt során használt ÖSSZES konstans
    /// </summary>
    public static class Consts
    {
        /// <summary>
        /// Felhasználónév ellenőrzéséhez regexp
        /// </summary>
        public const string FelhasznalonevRegexp = "^[a-zA-Z0-9_]*$";

        /// <summary>
        /// Jelszó ellenőrzéséhez regexp.
        /// </summary>
        public const string JelszoRegexp = "^[a-zA-Zá-űÁ-Ű0-9$#&@.:,?;!%=*-_<>|]*$";
    }
}
