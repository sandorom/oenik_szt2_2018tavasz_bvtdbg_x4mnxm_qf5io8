﻿// <copyright file="CsomagAllException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Common
{
    using System;
    using System.Runtime.Serialization;
    using System.Security.Permissions;

    /// <summary>
    /// CsomagAllException típusa, dobásának oka
    /// </summary>
    public enum CsomagAllExceptionTipus
    {
        /// <summary>
        /// Ha már létezik ilyen felhasználónév a DB-ben
        /// </summary>
        Letezo_Username,

        /// <summary>
        /// Nincs megadva felhasználónév/jelszó
        /// </summary>
        Ures_Felhasznalonev_Jelszo,

        /// <summary>
        /// Hibás állapotban van egy objektum.
        /// </summary>
        Inkonzisztens_Allapot,

        /// <summary>
        /// Sikertelen bejelentkezés. (Pl. hibás felhasználónév/jelszó)
        /// </summary>
        Sikertelen_Bejelentkezes,

        /// <summary>
        /// Ha egy megrendelés kiszállítás alatt van, akkor nem módosítható vagy törölhető.
        /// </summary>
        Termek_Kiszallitas_Alatt,

        /// <summary>
        /// Nem található adott paraméterű raktár
        /// </summary>
        Nem_Talalhato_Raktar,

        /// <summary>
        /// Nem sikerült rögzíteni a rendelést
        /// </summary>
        Sikertelen_Rendeles
    }

    /// <summary>
    /// Saját kivétel egyedi esetekre, jelzésekre
    /// </summary>
    [Serializable]
    public class CsomagAllException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CsomagAllException"/> class.
        /// </summary>
        /// <param name="tipus">Hiba típus</param>
        public CsomagAllException(CsomagAllExceptionTipus tipus)
        {
            this.Tipus = tipus;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CsomagAllException"/> class.
        /// </summary>
        /// <param name="tipus">Hiba típus</param>
        /// <param name="message">Egyedi üzenet</param>
        public CsomagAllException(CsomagAllExceptionTipus tipus, string message)
            : base(message)
        {
            this.Tipus = tipus;
        }

        /// <summary>
        /// Gets the type
        /// </summary>
        public CsomagAllExceptionTipus Tipus { get; }

        /// <summary>
        /// Serializable előírása
        /// </summary>
        /// <param name="info">Serializable info</param>
        /// <param name="context">StreamContext</param>
        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CsomagAllException", DateTime.Now);
            base.GetObjectData(info, context);
        }
    }
}
