﻿// <copyright file="Logger.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Common
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using Data;
    using Enums;
    using ViewModels;

    /// <summary>
    /// Általános logger osztály
    /// </summary>
    public abstract class Logger
    {
        /// <summary>
        /// Útvonal a bejelentkezett felhasználó asztaláig. Itt létrehozunk egy mappát, majd oda mentjük a logokat
        /// </summary>
        private static string logsFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/CsomagAll_logs";

        /// <summary>
        /// Megrendelő log-file neve.
        /// </summary>
        private static string megrendeloPath = "\\megrendelo.log";

        /// <summary>
        /// Raktárvezető log-file neve.
        /// </summary>
        private static string raktarVezetoPath = "\\raktarvezeto.log";

        /// <summary>
        /// Futár log-file neve.
        /// </summary>
        private static string futarPath = "\\futar.log";

        /// <summary>
        /// Megrendelő stream.
        /// </summary>
        private static StreamWriter megrendeloLogger;

        /// <summary>
        /// Raktárvezető stream.
        /// </summary>
        private static StreamWriter raktarVezetoLogger;

        /// <summary>
        /// Futár stream.
        /// </summary>
        private static StreamWriter futarLogger;

        /// <summary>
        /// Objektumok lockolásához használt elem.
        /// </summary>
        private static object lockObject;

        /// <summary>
        /// Létrehozza a log-fájlok mappáját, majd a log file-okat is, ha azok még nem lettek inicializálva.
        /// </summary>
        public static void Init()
        {
            Directory.CreateDirectory(logsFolderPath);
            megrendeloLogger = new StreamWriter(logsFolderPath + megrendeloPath, true, Encoding.UTF8);
            raktarVezetoLogger = new StreamWriter(logsFolderPath + raktarVezetoPath, true, Encoding.UTF8);
            futarLogger = new StreamWriter(logsFolderPath + futarPath, true, Encoding.UTF8);
            lockObject = new object();
            megrendeloLogger.Flush();
            raktarVezetoLogger.Flush();
            futarLogger.Flush();
        }

        /// <summary>
        /// Rendelés logolása.
        /// </summary>
        /// <param name="termekLista">Megrendelt termékek</param>
        /// <param name="sikertelenRendeles">Sikertelen rendelés</param>
        /// <param name="megrendelo">Megrendelő objektum</param>
        public static void RendelesLog(List<TermekRendeles> termekLista, List<string> sikertelenRendeles, MEGRENDELO megrendelo)
        {
            string log = LogDateTime();
            log += megrendelo.Nev + " (ID: " + megrendelo.ID + ") a következő ";
            string sikertelenLog = log;
            if (termekLista.Count == 1)
            {
                log += "terméket rendelte meg: ";
            }
            else
            {
                log += "termékeket rendelte meg: ";
            }

            foreach (TermekRendeles item in termekLista)
            {
                log += item.Mennyiseg + " db " + item.Termek.Nev + " (termék ID: " + item.Termek.ID + ", raktár ID: " + item.Raktar_Id + ") | ";
            }

            log += "A rendelés rögzítésre került!";
            sikertelenLog += "termék(ek)et NEM tudta megrendelni: ";
            foreach (string item in sikertelenRendeles)
            {
                sikertelenLog += item + " | ";
            }

            lock (lockObject)
            {
                megrendeloLogger.WriteLine(log);
                if (sikertelenRendeles.Count != 0)
                {
                    megrendeloLogger.WriteLine(sikertelenLog);
                }

                megrendeloLogger.Flush();
            }
        }

        /// <summary>
        /// Bejelentkezés logolása.
        /// </summary>
        /// <param name="nev">Bejelentkezett felhasználó teljes neve</param>
        /// <param name="id">Felhasználó azonosítója</param>
        /// <param name="jogosultsag">Felhasználó jogosultsága</param>
        public static void LoginLog(string nev, decimal id, JogosultsagEnum jogosultsag)
        {
            string log = LogDateTime();
            log += nev + " (ID: " + id + ") " + jogosultsag.ToString() + " bejelentkezett.";
            lock (lockObject)
            {
                switch (jogosultsag)
                {
                    case JogosultsagEnum.Futar: futarLogger.WriteLine(log); futarLogger.Flush(); break;
                    case JogosultsagEnum.Megrendelo: megrendeloLogger.WriteLine(log); megrendeloLogger.Flush(); break;
                    case JogosultsagEnum.Vezeto: raktarVezetoLogger.WriteLine(log); raktarVezetoLogger.Flush(); break;
                }
            }
        }

        /// <summary>
        /// Termék módosítás logolás.
        /// </summary>
        /// <param name="vezeto">Módosítást végző személy</param>
        /// <param name="termek">Módosított termék</param>
        public static void TermekModositasLog(RAKTARVEZETO vezeto, TERMEK termek)
        {
            string log = LogDateTime();
            log += vezeto.Nev + "(ID: " + vezeto.ID + ") módosította a(z) " + termek.Nev + " (ID: " + termek.ID + ") adatait.";
            lock (lockObject)
            {
                raktarVezetoLogger.WriteLine(log);
                raktarVezetoLogger.Flush();
            }
        }

        /// <summary>
        /// Új megrendelő/futár regisztráció utáni logolás.
        /// </summary>
        /// <param name="rv">Raktárvezető (futár esetán, egyébként null)</param>
        /// <param name="teljesNev">Felhasználó teljes neve</param>
        /// <param name="nev">Bejelentkezési név</param>
        /// <param name="irszam">Irányítószám (megrendelő esetén, egyébként string.Empty)</param>
        /// <param name="varos">Város (megrendelő esetén, egyébként string.Empty)</param>
        /// <param name="cim">Cím (megrendelő esetén, egyébként string.Empty)</param>
        /// <param name="rendszam">Szolgálati autó rendszáma (futár esetén, egyébként null)</param>
        public static void UJFelhasznaloRogzitesLog(RAKTARVEZETO rv, string teljesNev, string nev, string irszam, string varos, string cim, string rendszam)
        {
            string log = LogDateTime();
            if (rv == null)
            {
                log += "Megrendelő létrehozva a következő törzsadatokkal: ";
                log += teljesNev + "(username: " + nev + "), szállítási cím: " + irszam + " " + varos + " " + cim;
                lock (lockObject)
                {
                    megrendeloLogger.WriteLine(log);
                    megrendeloLogger.Flush();
                }
            }
            else
            {
                log += rv.Nev + " (ID: " + rv.ID + ") új futárt rögzített a következő adatokkal: ";
                log += teljesNev + "(username: " + nev + "), E-mail cím: " + nev + "@csomagall.com. A futárhoz tartozó szolgálati autó rendszáma: " + rendszam;
                lock (lockObject)
                {
                    raktarVezetoLogger.WriteLine(log);
                    raktarVezetoLogger.Flush();
                }
            }
        }

        /// <summary>
        /// Fuár - szállítás összerendelés logolás.
        /// </summary>
        /// <param name="rv">Összerendelést végző raktárvezető</param>
        /// <param name="futar">Kiszállítást végző futár</param>
        /// <param name="szallitas">Szállítás objektum</param>
        public static void FutarSzallitasOsszerendelesLog(RAKTARVEZETO rv, FUTAR futar, SZALLITAS szallitas)
        {
            string log = LogDateTime();
            log += rv.Nev + " (ID: " + rv.ID + ") raktárvezető a(z) " + futar.ID + " ID-jú futárhoz (rendszám: " + futar.Rendszam + ") hozzárendelte a(z) " + szallitas.ID + " ID-jú szállítást, ";
            log += " a kiszállítás kezdetét a következőre állította: " + DateTime.Now.ToShortDateString();
            lock (lockObject)
            {
                raktarVezetoLogger.WriteLine(log);
                raktarVezetoLogger.Flush();
            }
        }

        /// <summary>
        /// Futár elérhetőségének frissítése
        /// </summary>
        /// <param name="futar">Frissített futár objektum</param>
        public static void FutarElerhetosegValtozas(FUTAR futar)
        {
            string log = LogDateTime();
            log += futar.Nev + " (ID: " + futar.ID + ") megváltoztatta elérhetőségét a következőre: " + futar.Elerhetoseg;
            lock (lockObject)
            {
                futarLogger.WriteLine(log);
                futarLogger.Flush();
            }
        }

        /// <summary>
        /// Szállítás állapotfrissítés futár által
        /// </summary>
        /// <param name="futar">változást jelentő futár</param>
        /// <param name="szallitas">Frissített állapotú szállítás objektum</param>
        public static void SzallitasAllapotValtozas(FUTAR futar, SZALLITAS szallitas)
        {
            string log = LogDateTime();
            log += futar.Nev + " (ID: " + futar.ID + ") megváltoztatta a(z) " + szallitas.ID + " ID-jú szállítás állapotát a következőre: " + szallitas.Allapot;
            lock (lockObject)
            {
                futarLogger.WriteLine(log);
                futarLogger.Flush();
            }
        }

        /// <summary>
        /// A logok elejére kiírandó dátum és idő.
        /// </summary>
        /// <returns>Dátum és idő local culture alapján formázva.</returns>
        private static string LogDateTime()
        {
            return DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + ": ";
        }
    }
}
