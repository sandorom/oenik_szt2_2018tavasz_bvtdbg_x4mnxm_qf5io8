﻿// <copyright file="RaktarVezetoVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Authentication;
    using Common;
    using Data;
    using Data.Common;
    using Data.DAOs;
    using Data.Interfaces;
    using Enums;

    /// <summary>
    /// RaktárVezető folyamatai
    /// </summary>
    public class RaktarVezetoVM : ISzallitasKezeles, ITermekListazas, IUzenetkezeles
    {
        /// <summary>
        /// Termék adatbázis-műveletek
        /// </summary>
        private TermekDAO termekDAO = new TermekDAO();

        /// <summary>
        /// Szállítás adatbázis-műveletek
        /// </summary>
        private SzallitasDAO szallitasDAO = new SzallitasDAO();

        /// <summary>
        /// Üzenet adatbázis-műveletek
        /// </summary>
        private UzenetDAO uzenetDAO = new UzenetDAO();

        /// <summary>
        /// Futár adatbázis-műveletek
        /// </summary>
        private FutarDAO futarDAO = new FutarDAO();

        /// <summary>
        /// Regisztrálás folyamaok
        /// </summary>
        private RegisztracioService regisztracioService = new RegisztracioService();

        /// <summary>
        /// Initializes a new instance of the <see cref="RaktarVezetoVM"/> class.
        /// </summary>
        /// <param name="raktarVezeto">Bejelentkezett Vezeto</param>
        public RaktarVezetoVM(RAKTARVEZETO raktarVezeto)
        {
            this.RaktarVezeto = raktarVezeto;
            try
            {
                this.Raktar = this.szallitasDAO.RaktarByKorzet(raktarVezeto.Korzet.Value);
            }
            catch (InvalidOperationException)
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Nem_Talalhato_Raktar, "Nem található a(z) " + raktarVezeto.ID + " ID-jú raktár!");
            }
        }

        /// <summary>
        /// Gets logged Vezeto
        /// </summary>
        public RAKTARVEZETO RaktarVezeto { get; }

        /// <summary>
        /// Gets logged Vezeto raktára
        /// </summary>
        public RAKTAR Raktar { get; }

        /// <summary>
        /// Kiszámolja az aktuális hónap bevételét.
        /// </summary>
        /// <param name="raktarSzallitas">Raktár szállítás lista</param>
        /// <returns>Havi bevétel</returns>
        public static int HaviForgalom(List<SZALLITAS> raktarSzallitas)
        {
            if (raktarSzallitas == null || raktarSzallitas.Count == 0)
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Inkonzisztens_Allapot, "Érvénytelen szállítás lista!");
            }

            List<SZALLITAS> haviSzallitas = raktarSzallitas.Where(x => x.Szallitas_vege.Value.Year == DateTime.Now.Year
                                                                  && x.Szallitas_vege.Value.Month == DateTime.Now.Month
                                                                  && x.Allapot == SzallitasAllapotEnum.Kiszallitva.ToString()).ToList();
            return haviSzallitas.Sum(x => decimal.ToInt32((decimal)x.Fizetendo));
        }

        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        public void AllapotFrissites(SZALLITAS ujSzallitas)
        {
            this.szallitasDAO.AllapotFrissites(ujSzallitas);
        }

        /// <summary>
        /// Az összes raktár elérhető termékeinek visszaadása.
        /// </summary>
        /// <returns>Az összes raktár elérhető termékeinek visszaadása</returns>
        public List<TERMEK> ElerhetoTermekek()
        {
            return this.termekDAO.ElerhetoTermekek();
        }

        /// <summary>
        /// A raktárVezető raktárában elérhető termékek.
        /// </summary>
        /// <param name="raktar">A raktárVezető raktára</param>
        /// <returns>A raktárVezető raktárában elérhető termékek</returns>
        public List<TERMEK> ElerhetoTermekekRaktarSzerint(RAKTAR raktar)
        {
            return this.termekDAO.ElerhetoTermekekRaktarSzerint(raktar);
        }

        /// <summary>
        /// Szállítás objektumok közti szűrés.
        /// Ha valamire nem szeretnénk szűrni, akkor null értéket kell megadni!
        /// (string esetén: string.Empty)
        /// </summary>
        /// <param name="termekID">Szűréshez termék azonosító</param>
        /// <param name="raktarID">Szűréshez raktár azonosító</param>
        /// <param name="futarID">Szűréshez futár azonosító</param>
        /// <param name="allapot">Szállítás aktuális állapota</param>
        /// <param name="mennyiseg">Rendelt mennyiség</param>
        /// <returns>Szűrt szállítás lista</returns>
        public List<SZALLITAS> SzallitasSzures(int? termekID, int? raktarID, int? futarID, string allapot, int? mennyiseg)
        {
            return this.szallitasDAO.SzallitasSzures(termekID, raktarID, futarID, allapot, mennyiseg);
        }

        /// <summary>
        /// Felparaméterez és rögzít egy új üzenetet.
        /// </summary>
        /// <param name="cimzettAzonosito">A címzett azonosítója</param>
        /// <param name="targy">Az üzenet tárgya</param>
        /// <param name="tartalom">Az üzenet tartalma</param>
        public void UJUzenetParameterezesESKuldes(int cimzettAzonosito, string targy, string tartalom)
        {
            UZENET u = new UZENET()
            {
                Cimzett_ID = cimzettAzonosito,
                Kuldo_ID = this.RaktarVezeto.ID,
                Targy = targy,
                Tartalom = tartalom,
                Olvasatlan = 0,
                Beerkezett = DateTime.Now,
                ID = Math.Abs(targy.GetHashCode() - DateTime.Now.Second)
            };
            this.UjUzenet(u);
        }

        /// <summary>
        /// Rögzít egy új üzenetet az adatbázisba.
        /// </summary>
        /// <param name="uzenet">Felparaméterezett üzenet objektum</param>
        public void UjUzenet(UZENET uzenet)
        {
            this.uzenetDAO.UjUzenet(uzenet);
        }

        /// <summary>
        /// Felhasználó üzenetei
        /// </summary>
        /// <param name="id">Felhasználó saját azonosítója</param>
        /// <returns>Visszaadja a felhasználó összes üzenetét</returns>
        public List<UzenetForGui> FelhasznaloUzenetei(decimal id)
        {
            return this.uzenetDAO.FelhasznaloUzenetei(id);
        }

        /// <summary>
        /// A válaszhoz fel kell oldanunk a címzett nevét azonosító alapján
        /// </summary>
        /// <param name="id">Címzett azonosítója</param>
        /// <returns>Címzett neve</returns>
        public string CimzettNevFeloldas(decimal id)
        {
            return this.uzenetDAO.NevById(id);
        }

        /// <summary>
        /// Futárhoz szállítás rendelés
        /// </summary>
        /// <param name="szallitas">Szállítandó termékek</param>
        /// <param name="futar">Kiszállítást végző futár</param>
        public void SzallitasFutarOsszerendeles(SZALLITAS szallitas, FUTAR futar)
        {
            this.szallitasDAO.FutarhozRendeles(szallitas, futar);
            Task.Run(() => Logger.FutarSzallitasOsszerendelesLog(this.RaktarVezeto, futar, szallitas));
        }

        /// <summary>
        /// Raktár megrendelései
        /// </summary>
        /// <returns>Raktár kiszállításra váró megrendelései</returns>
        public List<SZALLITAS> Szallitasok()
        {
            return this.szallitasDAO.RaktarhozTartozoAktivSzallitasok(this.Raktar.ID);
        }

        /// <summary>
        /// Felhasználónév egyediség ellenőrzés, regex validálás, titkosítás és mentés.
        /// </summary>
        /// <param name="teljesNev">Felhasználó teljes neve</param>
        /// <param name="nev">Új felhasználó neve</param>
        /// <param name="jelszo">Jelszó</param>
        /// <param name="rendszam">Rendszám (csak futárnál, egyébként bármi lehet)</param>
        /// <param name="korzet">Körzet (csak futárnál, egyébként bármi lehet)</param>
        public void UJFutarFelvetele(string teljesNev, string nev, string jelszo, string rendszam, decimal? korzet)
        {
            this.regisztracioService.Regisztralas(teljesNev, nev, jelszo, JogosultsagEnum.Futar.ToString(), string.Empty, string.Empty, string.Empty, rendszam, korzet);
            Task.Run(() => Logger.UJFelhasznaloRogzitesLog(this.RaktarVezeto, teljesNev, nev, string.Empty, string.Empty, string.Empty, rendszam));
        }

        /// <summary>
        /// Meglévő termék módosítása.
        /// </summary>
        /// <param name="termek">Módosítitott adatokat tartalmazó objektum</param>
        public void TermekModositas(TERMEK termek)
        {
            this.termekDAO.TermekModositas(termek);
            Task.Run(() => Logger.TermekModositasLog(this.RaktarVezeto, termek));
        }

        /// <summary>
        /// Termék törlése. Valójában nem töröl, csak töröltre módosít.
        /// </summary>
        /// <param name="termek">Törölni kívánt termék</param>
        public void TermekTorlese(TERMEK termek)
        {
            termek.Ar = 0;
            termek.Leiras = "!!!TÖRÖLVE!!!";
            termek.Nev = "!!!TÖRÖLVE!!!";
            this.termekDAO.TermekModositas(termek);
            Task.Run(() => Logger.TermekModositasLog(this.RaktarVezeto, termek));
        }

        /// <summary>
        /// Egy új terméket rögzít az adatbázisban
        /// </summary>
        /// <param name="termek">Új termék</param>
        public void UjTermekMentes(TERMEK termek)
        {
            this.termekDAO.UjTermekMentes(termek);
        }

        /// <summary>
        /// Készlet növelés
        /// </summary>
        /// <param name="termek">Módosított termék</param>
        /// <param name="mennyiseg">Új mennyiség</param>
        public void KeszletNoveles(TERMEK termek, int mennyiseg)
        {
            this.termekDAO.KeszletModositas(decimal.ToInt32(this.Raktar.ID), decimal.ToInt32(termek.ID), mennyiseg);
        }

        /// <summary>
        /// Termék készlete
        /// </summary>
        /// <param name="termek">Válaszott termék</param>
        /// <returns>Visszaadja a termék mennyiségét</returns>
        public int TermekKeszleten(TERMEK termek)
        {
            return this.termekDAO.KeszletenLevoTermekRaktarban(decimal.ToInt32(this.Raktar.ID), decimal.ToInt32(termek.ID));
        }

        /// <summary>
        /// Futár listázás raktár szerint
        /// </summary>
        /// <returns>Raktár összes futárja</returns>
        public List<FUTAR> FutarokRaktarSzerint()
        {
            return this.futarDAO.FutarokRaktarSzerint(this.Raktar);
        }
    }
}
