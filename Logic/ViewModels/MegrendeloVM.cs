﻿// <copyright file="MegrendeloVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using Authentication;
    using Common;
    using Data;
    using Data.Common;
    using Data.DAOs;
    using Data.Interfaces;
    using Enums;

    /// <summary>
    /// Megrendelő ViewModel
    /// </summary>
    public class MegrendeloVM : Bindable, ITermekListazas, IUzenetkezeles
    {
        /// <summary>
        /// a megrendelő által megvásárolni kívánt termékek.
        /// </summary>
        private List<TermekRendeles> kosar;

        /// <summary>
        /// Termék szolgáltatások
        /// </summary>
        private TermekDAO termekDAO = new TermekDAO();

        /// <summary>
        /// Gets or sets szállítás DAO referencia
        /// </summary>
        private SzallitasDAO szallitasDAO = new SzallitasDAO();

        /// <summary>
        /// Üzenet adatbázis-műveletek
        /// </summary>
        private UzenetDAO uzenetDAO = new UzenetDAO();

        /// <summary>
        /// Regisztrálás folyamaok
        /// </summary>
        private RegisztracioService regisztracioService = new RegisztracioService();

        /// <summary>
        /// Initializes a new instance of the <see cref="MegrendeloVM"/> class.
        /// </summary>
        /// <param name="megrendelo">Bejelentkezett megrendelő</param>
        public MegrendeloVM(MEGRENDELO megrendelo)
        {
            this.Megrendelo = megrendelo;
            this.Kosar = new List<TermekRendeles>();
        }

        /// <summary>
        /// Gets logged Megrendelo
        /// </summary>
        public MEGRENDELO Megrendelo { get; }

        /// <summary>
        /// Gets vegosszeg
        /// </summary>
        public int VegOsszeg
        {
            get
            {
                int i = this.Kosar.Sum(x => decimal.ToInt32(x.Termek.Ar.Value) * x.Mennyiseg);
                this.Opc();
                return i;
            }
        }

        /// <summary>
        /// Gets or sets megrendelő kosár
        /// </summary>
        public List<TermekRendeles> Kosar
        {
            get
            {
                this.Opc();
                return this.kosar;
            }

            set
            {
                this.kosar = value;
                this.Opc();
            }
        }

        /// <summary>
        /// Rendelés törlése
        /// </summary>
        /// <param name="szallitas">Törlésre jelölt szállítás</param>
        public static void RendelesTorles(SZALLITAS szallitas)
        {
            if (string.Compare(SzallitasAllapotEnum.KiszallitasAlatt.ToString(), szallitas.Allapot, true) == 0)
            {
                throw new CsomagAllException(CsomagAllExceptionTipus.Termek_Kiszallitas_Alatt, "A megrendelés nem törölhető, mert kiszállítás alatt van!");
            }

            szallitas.Allapot = SzallitasAllapotEnum.Torolve.ToString();
        }

        /// <summary>
        /// Az összes raktár elérhető termékeinek visszaadása.
        /// </summary>
        /// <returns>Az összes raktár elérhető termékeinek visszaadása</returns>
        public List<TERMEK> ElerhetoTermekek()
        {
            return this.termekDAO.ElerhetoTermekek();
        }

        /// <summary>
        /// A raktárVezető raktárában elérhető termékek.
        /// </summary>
        /// <param name="raktar">A raktárVezető raktára</param>
        /// <returns>A raktárVezető raktárában elérhető termékek</returns>
        public List<TERMEK> ElerhetoTermekekRaktarSzerint(RAKTAR raktar)
        {
            return this.termekDAO.ElerhetoTermekekRaktarSzerint(raktar);
        }

        /// <summary>
        /// Kosárba helyezi a választott terméket!
        /// </summary>
        /// <param name="termek">Választott termék</param>
        /// <param name="mennyiseg">Termék mennyisége</param>
        /// <param name="raktarId">Választott raktár ID</param>
        public void KosarbaHelyez(TERMEK termek, int mennyiseg, int raktarId)
        {
            if (this.Kosar.Select(x => x.Termek).ToList().Contains(termek))
            {
                this.Kosar.Single(x => x.Termek == termek).Mennyiseg += mennyiseg;
            }
            else
            {
                this.Kosar.Add(new TermekRendeles()
                {
                    Termek = termek,
                    Mennyiseg = mennyiseg,
                    Raktar_Id = raktarId
                });
            }
        }

        /// <summary>
        /// Rögzíti a rendelést a kosárban található termékek alapján.
        /// </summary>
        public void Megrendel()
        {
            List<string> sikertelenRendeles = new List<string>();
            List<TermekRendeles> megrendeltTermekek = new List<TermekRendeles>();
            foreach (TermekRendeles item in this.Kosar)
            {
                try
                {
                    if (item.Mennyiseg <= this.termekDAO.KeszletenLevoTermekRaktarban(item.Raktar_Id, decimal.ToInt32(item.Termek.ID)))
                    {
                        SZALLITAS szallitas = new SZALLITAS()
                        {
                            ID = Math.Abs(this.Megrendelo.Nev.GetHashCode() - item.Termek.Nev.GetHashCode() + item.Mennyiseg),
                            Megrendelo_ID = this.Megrendelo.ID,
                            Termek_ID = item.Termek.ID,
                            Raktar_ID = item.Raktar_Id,
                            Futar_ID = 1, // nem létező ID-t adunk meg, azért, hogy meg tudjuk különböztetni (foreign key megkötés miatt kell neki ID)
                            Mennyiseg = item.Mennyiseg,
                            Fizetendo = item.Mennyiseg * item.Termek.Ar,
                            Allapot = SzallitasAllapotEnum.Megrendelve.ToString(),
                            Szallitas_kezdete = null,
                            Szallitas_vege = null
                        };
                        this.szallitasDAO.Rogzites(szallitas);
                        this.termekDAO.KeszletCsokkentes(item.Raktar_Id, decimal.ToInt32(item.Termek.ID), item.Mennyiseg);
                        megrendeltTermekek.Add(item);
                    }
                    else
                    {
                        sikertelenRendeles.Add(item.Termek.Nev + " (csökkentse a rendelt termék mennyiségét) ");
                    }
                }
                catch (DataException)
                {
                    sikertelenRendeles.Add(item.Termek.Nev);
                }
            }

            foreach (TermekRendeles item in megrendeltTermekek)
            {
                this.Kosar.RemoveAll(x => x.Termek.ID == item.Termek.ID);
            }

            Task.Run(() => Logger.RendelesLog(this.Kosar, sikertelenRendeles, this.Megrendelo));
            if (sikertelenRendeles.Count > 0)
            {
                string termekek = string.Empty;
                foreach (string item in sikertelenRendeles)
                {
                    termekek += item + " | ";
                }

                throw new CsomagAllException(CsomagAllExceptionTipus.Sikertelen_Rendeles, "Nem a sikerült a következő termék(ek) megrendelése: " + termekek + ".");
            }
        }

        /// <summary>
        /// Felparaméterez és rögzít egy új üzenetet.
        /// </summary>
        /// <param name="cimzettAzonosito">A címzett azonosítója</param>
        /// <param name="targy">Az üzenet tárgya</param>
        /// <param name="tartalom">Az üzenet tartalma</param>
        public void UJUzenetParameterezesESKuldes(int cimzettAzonosito, string targy, string tartalom)
        {
            UZENET u = new UZENET()
            {
                Cimzett_ID = cimzettAzonosito,
                Kuldo_ID = this.Megrendelo.ID,
                Targy = targy,
                Tartalom = tartalom,
                Olvasatlan = 0,
                Beerkezett = DateTime.Now,
                ID = Math.Abs(targy.GetHashCode() - DateTime.Now.Second)
            };
            this.UjUzenet(u);
        }

        /// <summary>
        /// Rögzít egy új üzenetet az adatbázisba.
        /// </summary>
        /// <param name="uzenet">Felparaméterezett üzenet objektum</param>
        public void UjUzenet(UZENET uzenet)
        {
            this.uzenetDAO.UjUzenet(uzenet);
        }

        /// <summary>
        /// A megrendelők ezeknek a raktárvezetőknek küldhetnek üzenetet.
        /// </summary>
        /// <returns>Összes raktárvezető</returns>
        public List<RAKTARVEZETO> RaktarVezetok()
        {
            return this.uzenetDAO.RaktarVezetok();
        }

        /// <summary>
        /// Felhasználó üzenetei
        /// </summary>
        /// <param name="id">Felhasználó saját azonosítója</param>
        /// <returns>Visszaadja a felhasználó összes üzenetét</returns>
        public List<UzenetForGui> FelhasznaloUzenetei(decimal id)
        {
            return this.uzenetDAO.FelhasznaloUzenetei(id);
        }

        /// <summary>
        /// A kosár tartalma alapján kiszámolja a végösszeget
        /// </summary>
        /// <returns>Összesen fizetendő</returns>
        public int Fizetendo()
        {
            int ossz = 0;
            foreach (TermekRendeles item in this.Kosar)
            {
                ossz += item.Mennyiseg * (int)item.Termek.Ar;
            }

            return ossz;
        }

        /// <summary>
        /// Ha megrendelt termék állapota nem kiszállítás alatt, akkor törölhető
        /// Törölt állapotúra állítja a megrendelést, de nem törli az adatbázisból
        /// </summary>
        /// <param name="szallitas">Törlendő szállítás objektum</param>
        public void RendelesTorlese(SZALLITAS szallitas)
        {
            RendelesTorles(szallitas);
            this.szallitasDAO.AllapotFrissites(szallitas);
        }

        /// <summary>
        /// Megrendelő rendelései
        /// </summary>
        /// <returns>A megrendelő nem törölt állapotú rendelései</returns>
        public List<SZALLITAS> Rendelesek()
        {
            return this.szallitasDAO.MegrendeloRendelesei(this.Megrendelo.ID);
        }

        /// <summary>
        /// Regisztrálás előtti validálás, majd mentés.
        /// </summary>
        /// <param name="teljesNev">Felhasználó teljes neve</param>
        /// <param name="username">Bejelentkezési név</param>
        /// <param name="jelszo">Jelszó</param>
        /// <param name="irszam">Irányítószám</param>
        /// <param name="varos">Város</param>
        /// <param name="cim">Szállítási cím</param>
        public void Regisztralas(string teljesNev, string username, string jelszo, string irszam, string varos, string cim)
        {
            this.regisztracioService.Regisztralas(teljesNev, username, jelszo, JogosultsagEnum.Megrendelo.ToString(), irszam, varos, cim, string.Empty, null);
            Task.Run(() => Logger.UJFelhasznaloRogzitesLog(null, teljesNev, username, irszam, varos, cim, string.Empty));
        }
    }
}
