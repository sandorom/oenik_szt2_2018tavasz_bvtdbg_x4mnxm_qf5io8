﻿// <copyright file="FutarVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.ViewModels
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common;
    using Data;
    using Data.DAOs;
    using Data.Interfaces;
    using Enums;

    /// <summary>
    /// Futar
    /// </summary>
    public class FutarVM : Bindable, IFutarKezeles
    {
        /// <summary>
        /// Futár DAO referencia
        /// </summary>
        private FutarDAO futarDAO = new FutarDAO();

        /// <summary>
        /// Szállítás DAO referencia
        /// </summary>
        private SzallitasDAO szallitasDAO = new SzallitasDAO();

        /// <summary>
        /// Gets or sets logged Futar
        /// </summary>
        private FUTAR futar;

        /// <summary>
        /// Initializes a new instance of the <see cref="FutarVM"/> class.
        /// </summary>
        /// <param name="futar">Logged futar</param>
        public FutarVM(FUTAR futar)
        {
            this.Futar = futar;
            this.SzallitasFrissites();
        }

        /// <summary>
        /// Gets or sets szállításra váró termékek
        /// </summary>
        public List<SZALLITAS> Szallitasok { get; set; }

        /// <summary>
        /// Gets or sets
        ///  </summary>
        public FUTAR Futar
        {
            get
            {
                this.Opc();
                return this.futar;
            }

            set
            {
                this.futar = value;
                this.Opc();
            }
        }

        /// <summary>
        /// Újratölti a futárhoz tartozó még NEM kézbesített szállításokat
        /// </summary>
        public void SzallitasFrissites()
        {
            this.Szallitasok = this.szallitasDAO.SzallitasSzures(null, null, decimal.ToInt32(this.Futar.ID), SzallitasAllapotEnum.KiszallitasAlatt.ToString(), null);
        }

        /// <summary>
        /// Szállítás állapotfrissítés
        /// </summary>
        /// <param name="ujSzallitas">Frissített állapotú szállítás objektum</param>
        public void AllapotFrissites(SZALLITAS ujSzallitas)
        {
            this.futarDAO.AllapotFrissites(ujSzallitas);
            Task.Run(() => Logger.SzallitasAllapotValtozas(this.Futar, ujSzallitas));
        }

        /// <summary>
        /// Futár elérhetőségének frissítése
        /// </summary>
        /// <param name="futar">Frissíteni kívánt futár</param>
        public void ElerhetosegFrissites(FUTAR futar)
        {
            this.futarDAO.ElerhetosegFrissites(futar);
            Task.Run(() => Logger.FutarElerhetosegValtozas(futar));
        }
    }
}
