﻿// <copyright file="FutarAllapotEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Enums
{
    /// <summary>
    /// A megrendelt termék állapotai.
    /// </summary>
    public enum FutarAllapotEnum
    {
        /// <summary>
        /// Elérhető állapot
        /// </summary>
        Elerheto,

        /// <summary>
        /// Túl sok rendelést kell kiszállítania
        /// </summary>
        Tulterhelt,

        /// <summary>
        /// Lebetegedett
        /// </summary>
        Beteg
    }
}
