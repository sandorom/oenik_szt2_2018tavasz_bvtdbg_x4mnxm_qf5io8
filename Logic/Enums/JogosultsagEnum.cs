﻿// <copyright file="JogosultsagEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Enums
{
    /// <summary>
    /// Beléptethető felhasználók.
    /// </summary>
    public enum JogosultsagEnum
    {
        /// <summary>
        /// RaktárVezető
        /// </summary>
        Vezeto,

        /// <summary>
        /// Futár
        /// </summary>
        Futar,

        /// <summary>
        /// Megrendelő
        /// </summary>
        Megrendelo
    }
}
