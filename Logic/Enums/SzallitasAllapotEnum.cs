﻿// <copyright file="SzallitasAllapotEnum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Logic.Enums
{
    /// <summary>
    /// A megrendelt termék állapotai.
    /// </summary>
    public enum SzallitasAllapotEnum
    {
        /// <summary>
        /// Megrendelt állapot
        /// </summary>
        Megrendelve,

        /// <summary>
        /// Kiszállítva
        /// </summary>
        Kiszallitva,

        /// <summary>
        /// Kiszállítás alatt
        /// </summary>
        KiszallitasAlatt,

        /// <summary>
        /// Törölt állapot
        /// </summary>
        Torolve,

        /// <summary>
        /// Sikertelen kézbesítés
        /// </summary>
        SikertelenKezbesites
    }
}
