﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic.ViewModels;
using Data;
using NUnit.Framework;
using Logic.Authentication;
using Logic.Enums;
using Data.DAOs;
using Moq;
using Logic.Common;


namespace Test 
{
    [TestFixture]
    public class LogicTest
    {
        //private MegrendeloVM mvm;
        //private FutarVM fvm;
        //private RaktarVezetoVM rvvm;

        //private MEGRENDELO megrendelo;
        //private FUTAR futar;
        //private RAKTARVEZETO rvezeto;

        //private RAKTAR raktar;
        //private TERMEK termek1;
        //private TERMEK termek2;
        //private TERMEK termek3;

        //[OneTimeSetUp]
        //public void Init()
        //{
        //    megrendelo = new MEGRENDELO() {Cim =" Budapest", ID=12, Iranyitoszam="1022",Nev="Bela",Varos="Budapest",SZALLITAS = new HashSet<SZALLITAS>() };
        //    futar = new FUTAR() { Elerhetoseg ="dfsd@sdfd" , ID= 8, Korzet=13, Nev="Zoltan", Rendszam="wer-123", SZALLITAS=  new HashSet<SZALLITAS>() };

        //    raktar = new RAKTAR() { Cim = "BP", ID = 57, Iranyitoszam = "1500", Korzet = 13, Varos = "BP", RaktarVezeto_ID = 34, KESZLET = new HashSet<KESZLET>() , SZALLITAS = new HashSet<SZALLITAS>()};

        //    rvezeto = new RAKTARVEZETO() { ID = 34, Korzet = 13, Nev = "Laszlo", RAKTAR = new HashSet<RAKTAR>() { raktar } };

        //    termek1 = new TERMEK() { Ar = 1230 , ID= 7, Leiras="Jóó", Nev="Konyv", Suly=5 , SZALLITAS = new HashSet<SZALLITAS>() , KESZLET = new HashSet<KESZLET>() { new KESZLET() {ID=92, Keszlet_Mennyiseg= 56, RAKTAR = raktar,Raktar_ID= raktar.ID, Termek_ID = 7 } } };
        //    termek2 = new TERMEK() { Ar = 547, ID = 6, Leiras = "Jó", Nev = "Konyv2", Suly = 5, SZALLITAS = new HashSet<SZALLITAS>(), KESZLET = new HashSet<KESZLET>() { new KESZLET() { ID = 76, Keszlet_Mennyiseg = 56, RAKTAR = raktar, Raktar_ID = raktar.ID, Termek_ID = 6 } } };
        //    termek3 = new TERMEK() { Ar = 8736, ID = 5, Leiras = "Szuper", Nev = "Konyv3", Suly = 5, SZALLITAS = new HashSet<SZALLITAS>(), KESZLET = new HashSet<KESZLET>() { new KESZLET() { ID = 67, Keszlet_Mennyiseg = 56, RAKTAR = raktar, Raktar_ID = raktar.ID, Termek_ID = 5 } } };



        //    this.mvm = new MegrendeloVM(megrendelo);
        //    this.fvm = new FutarVM(futar);
        //    this.rvvm = new RaktarVezetoVM(rvezeto);

        //}

        private Mock<ICsomagAllRepository> RepositoryLetrehozas()
        {
            Mock<ICsomagAllRepository> csomagAllRepo = new Mock<ICsomagAllRepository>();
            List<MEGRENDELO> megrendelok = new List<MEGRENDELO>();
            List<TERMEK> termekek = new List<TERMEK>();
            List<RAKTARVEZETO> vezetok = new List<RAKTARVEZETO>();
            List<FUTAR> futarok = new List<FUTAR>();
            List<KESZLET> keszletek = new List<KESZLET>();
            List<RAKTAR> raktarak = new List<RAKTAR>();
            List<SZALLITAS> szallitasok = new List<SZALLITAS>();

            // MEGRENDELŐK felvétele
            megrendelok.Add(new MEGRENDELO() { Cim = "Váci út 26.", Iranyitoszam = "1134", Nev = "Kiss Péter", Varos = "Budapest", ID = 1 });
            megrendelok.Add(new MEGRENDELO() { Cim = "Bethlen utca 12/A.", Iranyitoszam = "2586", Nev = "Nagy Anita", Varos = "Győr", ID = 2 });
            megrendelok.Add(new MEGRENDELO() { Cim = "Nevenincs tér 9.", Iranyitoszam = "6431", Nev = "Széles Szabolcs", Varos = "Szeged", ID = 3 });

            // TERMÉKEK felvétele
            termekek.Add(new TERMEK() { Ar = 500, Leiras = "Leírás 1", Nev = "Termék 1", Suly = 1, ID = 4 });
            termekek.Add(new TERMEK() { Ar = 1000, Leiras = "Leírás 2", Nev = "Termék 2", Suly = 3, ID = 5 });
            termekek.Add(new TERMEK() { Ar = 1500, Leiras = "Leírás 3", Nev = "Termék 3", Suly = 4, ID = 6 });

            // RAKTÁRAK felvétele
            raktarak.Add(new RAKTAR() { ID = 21, RaktarVezeto_ID = 9, Korzet = 10, Iranyitoszam = "1135", Varos = "Budapest", Cim = "Dózsa György út 142." });
            raktarak.Add(new RAKTAR() { ID = 22, RaktarVezeto_ID = 7, Korzet = 20, Iranyitoszam = "5311", Varos = "Szolnok", Cim = "Hegyalja út 26." });
            raktarak.Add(new RAKTAR() { ID = 23, RaktarVezeto_ID = 8, Korzet = 30, Iranyitoszam = "4949", Varos = "Győr", Cim = "Ferde köz 2." });


            // RAKTÁRVEZETŐK felvétele
            vezetok.Add(new RAKTARVEZETO() { ID = 9, Nev = "Profi Péter", Korzet = 10 });
            vezetok.Add(new RAKTARVEZETO() { ID = 7, Nev = "Hihetetlen Henrik", Korzet = 20 });
            vezetok.Add(new RAKTARVEZETO() { ID = 8, Nev = "Csinos Csilla", Korzet = 30 });

            // FUTÁROK létrehozása
            futarok.Add(new FUTAR() { ID = 10, Korzet = 10, Nev = "Sebes Sebestyén", Rendszam = "SBS101", Elerhetoseg = FutarAllapotEnum.Elerheto.ToString() });
            futarok.Add(new FUTAR() { ID = 11, Korzet = 20, Nev = "Gyors Gyula", Rendszam = "GYG201", Elerhetoseg = FutarAllapotEnum.Elerheto.ToString() });
            futarok.Add(new FUTAR() { ID = 12, Korzet = 30, Nev = "Lomha László", Rendszam = "LML301", Elerhetoseg = FutarAllapotEnum.Beteg.ToString() });

            // KÉSZLETEK feltöltése
            keszletek.Add(new KESZLET() { ID = 1, Raktar_ID = 10, Termek_ID = 4, Keszlet_Mennyiseg = 3 });
            keszletek.Add(new KESZLET() { ID = 2, Raktar_ID = 10, Termek_ID = 5, Keszlet_Mennyiseg = 2 });
            keszletek.Add(new KESZLET() { ID = 3, Raktar_ID = 10, Termek_ID = 6, Keszlet_Mennyiseg = 1 });
            keszletek.Add(new KESZLET() { ID = 4, Raktar_ID = 20, Termek_ID = 4, Keszlet_Mennyiseg = 6 });
            keszletek.Add(new KESZLET() { ID = 5, Raktar_ID = 20, Termek_ID = 5, Keszlet_Mennyiseg = 9 });
            keszletek.Add(new KESZLET() { ID = 6, Raktar_ID = 20, Termek_ID = 6, Keszlet_Mennyiseg = 3 });
            keszletek.Add(new KESZLET() { ID = 7, Raktar_ID = 30, Termek_ID = 4, Keszlet_Mennyiseg = 0 });
            keszletek.Add(new KESZLET() { ID = 8, Raktar_ID = 30, Termek_ID = 5, Keszlet_Mennyiseg = 1 });
            keszletek.Add(new KESZLET() { ID = 9, Raktar_ID = 30, Termek_ID = 6, Keszlet_Mennyiseg = 0 });

            // SZÁLLÍTÁSOK rögzítése
            szallitasok.Add(new SZALLITAS() { ID = 100, Allapot = SzallitasAllapotEnum.Megrendelve.ToString(), Megrendelo_ID = 1, Mennyiseg = 1, Szallitas_kezdete = DateTime.Now, Raktar_ID = 21, Termek_ID = 4, Fizetendo = 500 });
            szallitasok.Add(new SZALLITAS() { ID = 101, Allapot = SzallitasAllapotEnum.KiszallitasAlatt.ToString(), Megrendelo_ID = 2, Mennyiseg = 3, Szallitas_kezdete = new DateTime(2018, 4, 18), Raktar_ID = 23, Termek_ID = 6, Fizetendo = 4500 });
            szallitasok.Add(new SZALLITAS() { ID = 102, Allapot = SzallitasAllapotEnum.Kiszallitva.ToString(), Megrendelo_ID = 3, Mennyiseg = 5, Szallitas_kezdete = new DateTime(2018, 1, 29), Raktar_ID = 22, Termek_ID = 5, Fizetendo = 5000, Szallitas_vege = new DateTime(2018, 2, 5), Futar_ID = 10 });
            szallitasok.Add(new SZALLITAS() { ID = 103, Allapot = SzallitasAllapotEnum.Torolve.ToString(), Megrendelo_ID = 3, Mennyiseg = 4, Szallitas_kezdete = new DateTime(2018, 5, 1), Raktar_ID = 21, Termek_ID = 6, Fizetendo = 6000 });
            szallitasok.Add(new SZALLITAS() { ID = 104, Allapot = SzallitasAllapotEnum.SikertelenKezbesites.ToString(), Megrendelo_ID = 2, Mennyiseg = 1, Szallitas_kezdete = new DateTime(2018, 4, 28), Raktar_ID = 21, Termek_ID = 4, Fizetendo = 500, Futar_ID = 12, Szallitas_vege = new DateTime(2018, 5, 3) });

            csomagAllRepo.Setup(x => x.GetAllFutar()).Returns(futarok.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllKeszlet()).Returns(keszletek.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllMegrendelo()).Returns(megrendelok.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllRaktar()).Returns(raktarak.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllRaktarVezeto()).Returns(vezetok.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllSzallitas()).Returns(szallitasok.AsQueryable());
            csomagAllRepo.Setup(x => x.GetAllTermek()).Returns(termekek.AsQueryable());

            csomagAllRepo.Object.GetAllSzallitas().Where(x => x.Allapot == SzallitasAllapotEnum.KiszallitasAlatt.ToString()).FirstOrDefault();


            return csomagAllRepo;
        }


        //---------------------LogonService Tesztek 

        [TestCase("guest","guest01" )]
        [TestCase("user", "user01")]
        public void LoginFailedTest(string f, string j)
        {
            Assert.That(() => LoginService.Bejelentkezes(f,j),Throws.InvalidOperationException );
        }


       
        //------------------TitkositasService tesztek

        //hasonló szavakra nem ugyan azt

       
        [TestCase("teszt", "teszt")]
        [TestCase("proba", "proba")]
        public void EncryptTestSameTrue(string egyik, string masik)
        {
            string elso = TitkositasService.Encrypt(egyik);
            string masodik = TitkositasService.Encrypt(masik);

            Assert.AreEqual(elso, masodik);
        }

        [TestCase("jelszo", "Jelszo")]
        [TestCase("password", "pasword")]       
        public void EncryptTestSameFalse(string egyik, string masik)
        {
            string elso = TitkositasService.Encrypt(egyik);
            string masodik = TitkositasService.Encrypt(masik);

           
            Assert.AreNotEqual(elso, masodik);
        }

        





        // --------------------HASH - Nem az alapszó karaktereit módosítja a = @, o = 0, első betű nagy..stb

        [TestCase("jelszo", "JeŁ$z0")]
        [TestCase("password", "P@$$w0rĐ")]

        public void EncryptTestTrueHashOk(string egyik, string masik)
        {
            string elso = TitkositasService.Encrypt(egyik);
                       
            Assert.AreNotEqual(elso,masik);
                       
        }

      


        // -------------------------- Kodolás

        /* "teszt" "/pVNdrLWBDCAalw9K4Qg0A=="
        "password" "JGrkOgAWTsJIjEGmqkE9Lw=="
        "pasword"  "qcMhTiZuiTCzYviQv+gmpg=="
        "jelszo" "U8JjYQywkC62WMD1UybN4A=="
        "Jelszo" "lk1CKbw1dVdpr1Ag9BNRcw=="
        */



        [TestCase("jelszo", "U8JjYQywkC62WMD1UybN4A==")]
        [TestCase("password", "JGrkOgAWTsJIjEGmqkE9Lw==")]      
        public void EncryptTestTitkosOK(string egyik, string masik)
        {
            string titkos = TitkositasService.Encrypt(egyik);          

            Assert.That(titkos, Is.EqualTo(masik));
        }


       
        [TestCase("password", "qcMhTiZuiTCzYviQv+gmpg==")]
        [TestCase("teszt", "U8JjYQywkC62WMD1UybN4A==")]
        public void EncryptTestTitkosNO(string egyik, string masik)
        {
            string titkos = TitkositasService.Encrypt(egyik);

            Assert.That(titkos, !Is.EqualTo(masik));
        }


        //-------------------- Ures

        [TestCase("guest", "")]
        [TestCase("", "user01")]
     
        public void LoginEmptyNullTest(string f, string j)
        {
           // Exception ex = new CsomagAllException(CsomagAllExceptionTipus.Sikertelen_Bejelentkezes, "Helytelen felhasználónév vagy jelszó! Kérjük próbálja újra!");

            if (f == null || j == null )
            {
               Assert.That(() => LoginService.Bejelentkezes(f, j),Throws.ArgumentException);
               
            }
            
        }

        //---------------------Szallitas Frissítése

        [TestCase("Kiszallitva", "KiszallitasAlatt")]
        public static void SzallitasFrissitesTeszt(string a, string b)
        {
            SZALLITAS ujSzallitas = new SZALLITAS();
            ujSzallitas.Allapot = a;

            SZALLITAS regiSzallitas = new SZALLITAS();
            regiSzallitas.Allapot = b;

            regiSzallitas.Allapot = ujSzallitas.Allapot;
            if (SzallitasAllapotEnum.Kiszallitva.Equals(ujSzallitas.Allapot))
            {
                Assert.AreEqual(regiSzallitas.Szallitas_vege, DateTime.Now);
            }
        }

        [TestCase("Kiszallitva", "KiszallitasAlatt")]
        public static void SzallitasFrissitesTest2(string a, string b)
        {
            SZALLITAS ujSzallitas = new SZALLITAS();
            ujSzallitas.Allapot = a;

            SZALLITAS regiSzallitas = new SZALLITAS();
            regiSzallitas.Allapot = b;

            regiSzallitas.Allapot = ujSzallitas.Allapot;
            if (SzallitasAllapotEnum.Kiszallitva.Equals(ujSzallitas.Allapot))
            {
                Assert.AreNotEqual(regiSzallitas.Szallitas_vege, new DateTime(2017, 12, 25));
            }
        }

        [TestCase("SikertelenKezbesites", "KiszallitasAlatt")]
        public static void SzallitasFrissitesTeszt3(string a, string b)
        {
            SZALLITAS ujSzallitas = new SZALLITAS();
            ujSzallitas.Allapot = a;

            SZALLITAS regiSzallitas = new SZALLITAS();
            regiSzallitas.Allapot = b;

            regiSzallitas.Allapot = ujSzallitas.Allapot;
            if (SzallitasAllapotEnum.SikertelenKezbesites.Equals(ujSzallitas.Allapot))
            {
                Assert.AreEqual(regiSzallitas.Szallitas_vege, DateTime.Now);
            }
        }


        [TestCase("SikertelenKezbesites", "KiszallitasAlatt")]
        public static void SzallitasFrissitesTest4(string a, string b)
        {
            SZALLITAS ujSzallitas = new SZALLITAS();
            ujSzallitas.Allapot = a;

            SZALLITAS regiSzallitas = new SZALLITAS();
            regiSzallitas.Allapot = b;

            regiSzallitas.Allapot = ujSzallitas.Allapot;
            if (SzallitasAllapotEnum.SikertelenKezbesites.Equals(ujSzallitas.Allapot))
            {
                Assert.AreNotEqual(regiSzallitas.Szallitas_vege, new DateTime(2017, 12, 25));
            }
        }


    }
}
