﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    interface ICsomagAllRepository
    {
        IQueryable<MEGRENDELO> GetAllMegrendelo();
        IQueryable<KESZLET> GetAllKeszlet();
        IQueryable<TERMEK> GetAllTermek();
        IQueryable<RAKTAR> GetAllRaktar();
        IQueryable<SZALLITAS> GetAllSzallitas();
        IQueryable<FUTAR> GetAllFutar();
        IQueryable<RAKTARVEZETO> GetAllRaktarVezeto();
    }
}
