var searchData=
[
  ['sikeresbejelentkezes',['SikeresBejelentkezes',['../class_data_1_1_authentication_1_1_login_d_a_o.html#a6b8d2f6c633e32a34bcc23198309186e',1,'Data::Authentication::LoginDAO']]],
  ['sikertelen_5fbejelentkezes',['Sikertelen_Bejelentkezes',['../namespace_logic_1_1_common.html#a926c119bb590cb24cc920124e5a34882affdfba97b6f822f3882672f3d0a5684e',1,'Logic::Common']]],
  ['sikertelen_5frendeles',['Sikertelen_Rendeles',['../namespace_logic_1_1_common.html#a926c119bb590cb24cc920124e5a34882a422bf2f991b7ab376e629e3d559bfbb9',1,'Logic::Common']]],
  ['sikertelenkezbesites',['SikertelenKezbesites',['../namespace_logic_1_1_enums.html#a9b6c174f456020acad3ad3649fc61bf5a514fdd78d3706be5a201d4f7bf4d76be',1,'Logic::Enums']]],
  ['szallitas',['SZALLITAS',['../class_data_1_1_s_z_a_l_l_i_t_a_s.html',1,'Data']]],
  ['szallitasallapotenum',['SzallitasAllapotEnum',['../namespace_logic_1_1_enums.html#a9b6c174f456020acad3ad3649fc61bf5',1,'Logic::Enums']]],
  ['szallitasallapotvaltozas',['SzallitasAllapotValtozas',['../class_logic_1_1_common_1_1_logger.html#a2a4df09b6241e934a6d5697708c8fa39',1,'Logic::Common::Logger']]],
  ['szallitasdao',['SzallitasDAO',['../class_data_1_1_d_a_os_1_1_szallitas_d_a_o.html',1,'Data::DAOs']]],
  ['szallitasfrissites',['SzallitasFrissites',['../class_data_1_1_d_a_os_1_1_szallitas_d_a_o.html#a7d2091cf7cbcb01daaf5ce3610aac6cc',1,'Data.DAOs.SzallitasDAO.SzallitasFrissites()'],['../class_logic_1_1_view_models_1_1_futar_v_m.html#a905092ad6fec4538a8c9cdfbffae4f54',1,'Logic.ViewModels.FutarVM.SzallitasFrissites()']]],
  ['szallitasfutarosszerendeles',['SzallitasFutarOsszerendeles',['../class_logic_1_1_view_models_1_1_raktar_vezeto_v_m.html#af357c7de24130abed891ac492e56e524',1,'Logic::ViewModels::RaktarVezetoVM']]],
  ['szallitasok',['Szallitasok',['../class_logic_1_1_view_models_1_1_futar_v_m.html#a58075cf0c144adcad452d00ba11f8da8',1,'Logic.ViewModels.FutarVM.Szallitasok()'],['../class_logic_1_1_view_models_1_1_raktar_vezeto_v_m.html#ab878b02346712ded576b8f0e5cc55777',1,'Logic.ViewModels.RaktarVezetoVM.Szallitasok()']]],
  ['szallitasszures',['SzallitasSzures',['../class_data_1_1_d_a_os_1_1_szallitas_d_a_o.html#ac7ce4d24f1e7b5048e31083e07a95217',1,'Data.DAOs.SzallitasDAO.SzallitasSzures()'],['../interface_data_1_1_interfaces_1_1_i_szallitas_kezeles.html#afda5ef429b4bed3370165caab604dd59',1,'Data.Interfaces.ISzallitasKezeles.SzallitasSzures()'],['../class_logic_1_1_view_models_1_1_raktar_vezeto_v_m.html#a9b6059cb37e542ddf8f6b00bde6da656',1,'Logic.ViewModels.RaktarVezetoVM.SzallitasSzures()']]],
  ['szoveg',['Szoveg',['../class_data_1_1_common_1_1_uzenet_for_gui.html#a5a6b70b97d388906ed3d48cbcb1cb338',1,'Data::Common::UzenetForGui']]]
];
