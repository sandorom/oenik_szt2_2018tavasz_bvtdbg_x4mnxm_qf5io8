vez.xam.cs:

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CsomagALL
{
    /// <summary>
    /// Interaction logic for Vezeto.xaml
    /// </summary>
    public partial class Vezeto : Window
    {
        public Vezeto()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            rendelesek_sp.Visibility = Visibility.Collapsed;
            futarok_sp.Visibility = Visibility.Collapsed;
            termekek_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            termekek_sp.Visibility = Visibility.Collapsed;
            futarok_sp.Visibility = Visibility.Collapsed;
            rendelesek_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            rendelesek_sp.Visibility = Visibility.Collapsed;
            termekek_sp.Visibility = Visibility.Collapsed;
            futarok_sp.Visibility = Visibility.Visible;
        }
    }
}

vez.xam:

<Window x:Class="CsomagALL.Vezeto"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:CsomagALL"
        mc:Ignorable="d"
        ResizeMode="CanMinimize"
        Title="Vezeto" Height="522.038" Width="723.477" FontSize="14" Background="#04c582" Closing="Window_Closing">
    <Border Background="#2e3137" CornerRadius="20" Margin="20">
        <StackPanel Margin="20,20,20,10">
            <Menu Background="#2e3137" Foreground="White">
                <MenuItem Header="Term�kek" Click="MenuItem_Click">
                </MenuItem>
                <MenuItem Header="Megrendel�sek" Click="MenuItem_Click_1">
                </MenuItem>
                <MenuItem Header="Fut�rok" Click="MenuItem_Click_2">
                </MenuItem>
            </Menu>

            <Separator></Separator>

            <StackPanel x:Name="termekek_sp" Orientation="Horizontal" Height="387" Visibility="Visible">
                <ListBox x:Name="osszesTermek_lb" Background="#545d6a" Width="293" Margin="10"/>
                <StackPanel Width="317">
                    <Label x:Name="termekNev_label" Content="N�v" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekNev_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekAr_label" Content="�r" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekAr_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekLeiras_label" Content="Le�r�s" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekLeiras_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" Height="93" />
                    <StackPanel Orientation="Horizontal">
                        <Button x:Name="torles_btn" Content="T�rl�s" Margin="10 10 10 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                        <Button x:Name="szerk_btn" Content="Szerkeszt�s" Margin="10 10 10 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                    </StackPanel>
                </StackPanel>
            </StackPanel>

            <StackPanel x:Name="rendelesek_sp" Orientation="Horizontal" Height="387" Visibility="Collapsed">
                <ListBox x:Name="rendelesek_lb" Background="#545d6a" Width="190" Margin="10"/>
                <ListBox x:Name="rendelestermekei_lb" Background="#545d6a" Width="190" Margin="10"/>
            </StackPanel>

            <StackPanel x:Name="futarok_sp" Orientation="Horizontal" Height="387" Visibility="Collapsed">
                <ListBox x:Name="futarok_lb" Background="#545d6a" Width="190" Margin="10"/>
            </StackPanel>

        </StackPanel>
    </Border>
</Window>

megr.xam.cs:

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CsomagALL
{
    public partial class Megrendelo : Window
    {
        public Megrendelo()
        {
            InitializeComponent();
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            kosar_sp.Visibility = Visibility.Collapsed;
            uzenet_sp.Visibility = Visibility.Collapsed;
            rendelesek_sp.Visibility = Visibility.Collapsed;

            termekek_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            termekek_sp.Visibility = Visibility.Collapsed;
            uzenet_sp.Visibility = Visibility.Collapsed;
            rendelesek_sp.Visibility = Visibility.Collapsed;

            kosar_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            kosar_sp.Visibility = Visibility.Collapsed;
            termekek_sp.Visibility = Visibility.Collapsed;
            rendelesek_sp.Visibility = Visibility.Collapsed;

            uzenet_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            kosar_sp.Visibility = Visibility.Collapsed;
            uzenet_sp.Visibility = Visibility.Collapsed;
            termekek_sp.Visibility = Visibility.Collapsed;

            rendelesek_sp.Visibility = Visibility.Visible;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}

megr.xam:

<Window x:Class="CsomagALL.Megrendelo"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:CsomagALL"
        mc:Ignorable="d"
        ResizeMode="CanMinimize"
        Title="Megrendelo" Height="522.038" Width="723.477" FontSize="14" Background="#04c582" Closing="Window_Closing">
    <Border Background="#2e3137" CornerRadius="20" Margin="20">
        <StackPanel Margin="20,20,20,10">
            <Menu Background="#2e3137" Foreground="White">
                <MenuItem Header="Term�kek" Click="MenuItem_Click">
                </MenuItem>
                <MenuItem Header="Kos�r" Click="MenuItem_Click_1">
                </MenuItem>
                <MenuItem Header="�zenetek" Click="MenuItem_Click_2">
                </MenuItem>
                <MenuItem Header="Rendel�seim" Click="MenuItem_Click_3">
                </MenuItem>
            </Menu>

            <Separator></Separator>
            
            <StackPanel x:Name="termekek_sp" Orientation="Horizontal" Height="387" Visibility="Visible">
                <ListBox x:Name="osszesTermek_lb" Background="#545d6a" Width="293" Margin="10"/>
                <StackPanel Width="317">
                    <Label x:Name="termekNev_label" Content="N�v" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekNev_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekAr_label" Content="�r" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekAr_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekLeiras_label" Content="Le�r�s" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekLeiras_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" Height="93" />
                    <Button x:Name="kosarba_btn" Content="Kos�rba" Margin="80 10 80 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>
            
            <StackPanel x:Name="kosar_sp" Orientation="Horizontal" Height="387" Visibility="Collapsed">
                <StackPanel>
                    <ListBox x:Name="kosarTermek_lb" Background="#545d6a" Width="293" Margin="10" Height="320"/>
                    <StackPanel Orientation="Horizontal">
                        <Label x:Name="teljesAr_label_kosar" Content="V�g�sszeg:" Background="#545d6a" Margin="10 10 5 10" Foreground="White"/>
                        <TextBox x:Name="teljesAr_tb_kosar" Background="#545d6a" Foreground="White" Margin="5 10 10 10" Width="204" />
                    </StackPanel>
                </StackPanel>
                <StackPanel Width="317">
                    <Label x:Name="termekNev_label_kosar" Content="N�v" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekNev_tb_kosar" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekAr_label_kosar" Content="�r" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekAr_tb_kosar" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="termekLeiras_label_kosar" Content="Le�r�s" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="termekLeiras_tb_kosar" Background="#545d6a" Foreground="White" Margin="10 10 10 10" Height="93" />
                    <Button x:Name="rendel_btn_kosar" Content="Megrendel" Margin="80 10 80 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>

            <StackPanel x:Name="uzenet_sp" Orientation="Horizontal" Height="387" Visibility="Collapsed">
                <StackPanel>
                    <ListBox x:Name="uzenetek_lb" Background="#545d6a" Width="293" Margin="10" Height="320"/>
                    <StackPanel Orientation="Horizontal" HorizontalAlignment="Center">
                        <Button x:Name="torles_btn" Content="T�rl�s" Margin="10 10 10 10" Background="#545d6a" Foreground="White" FontSize="18" Width="64"/>
                    </StackPanel>
                </StackPanel>
                <StackPanel Width="317">
                    <Label x:Name="felado_label" Content="Felad�" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="felado_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="targy_label" Content="T�rgy" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="targy_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="szoveg_label" Content="�zenet sz�vege" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="szoveg_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" Height="93" />
                    <Button x:Name="valasz_btn" Content="V�lasz" Margin="80 10 80 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>

            <StackPanel x:Name="rendelesek_sp" Orientation="Horizontal" Height="387" Visibility="Collapsed">
                <StackPanel>
                    <ListBox x:Name="rendelesek_lb" Background="#545d6a" Width="293" Margin="10" Height="369"/> 
                </StackPanel>
                <StackPanel Width="317">
                    <Label x:Name="rendelesId_label" Content="Azonos�t�" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="rendelesId_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="allapot_label" Content="�llapot" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="allapot_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Label x:Name="fizetendo_label" Content="Fizetend� �sszeg" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <TextBox x:Name="fizetendo_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                    <Button x:Name="torles_btn_rendeles" Content="Rendel�s t�rl�se" Margin="80 10 80 10" Background="#545d6a" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>

        </StackPanel>
    </Border>
</Window>

fut.xam.cs:

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CsomagALL
{
    /// <summary>
    /// Interaction logic for Futar.xaml
    /// </summary>
    public partial class Futar : Window
    {
        public Futar()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            allapot_sp.Visibility = Visibility.Collapsed;
            csomagok_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            csomagok_sp.Visibility = Visibility.Collapsed;
            allapot_sp.Visibility = Visibility.Visible;
        }
    }
}

fut.xam:

<Window x:Class="CsomagALL.Futar"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:CsomagALL"
        mc:Ignorable="d"
        ResizeMode="CanMinimize"
        Title="Futar" Height="543.038" Width="558.477" FontSize="14" Background="#04c582" Closing="Window_Closing">
    <Border Background="#2e3137" CornerRadius="20" Margin="20">
        <StackPanel Margin="20,20,20,10">
            <Menu Background="#2e3137" Foreground="White">
                <MenuItem Header="Csomagok" Click="MenuItem_Click">
                </MenuItem>
                <MenuItem Header="�llapot" Click="MenuItem_Click_1">
                </MenuItem>
            </Menu>

            <Separator></Separator>

            <StackPanel x:Name="csomagok_sp" Height="461" Visibility="Visible">
                <Label x:Name="rendeloNev_label" Content="Megrendel� neve:" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                <TextBox x:Name="rendeloNev_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                <Label x:Name="rendeloCim_label" Content="C�me:" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                <TextBox x:Name="rendelocim_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                <Label x:Name="RendelesID_label" Content="Term�k ID:" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                <TextBox x:Name="RendelesID_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                <Label x:Name="fiz_label" Content="Fizetend�:" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                <TextBox x:Name="fiz_tb" Background="#545d6a" Foreground="White" Margin="10 10 10 10" />
                <StackPanel Orientation="Horizontal" HorizontalAlignment="Center">
                    <Button x:Name="sikeres_btn" Content="Sikeres kisz�ll�t�s" Margin="10 10 10 10" Background="Green" Foreground="White" FontSize="18"/>
                    <Button x:Name="sikertelen_btn" Content="Sikertelen kisz�ll�t�s" Margin="10 10 10 10" Background="Red" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>

            <StackPanel x:Name="allapot_sp" Orientation="Vertical" Height="387" Visibility="Collapsed">
                <StackPanel Orientation="Horizontal" HorizontalAlignment="Center">
                    <Label x:Name="allapot_label" Content="�llapot:" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                    <Label x:Name="allapotadatkotve_label" Content="" Background="#545d6a" Margin="10 10 10 10" Foreground="White"/>
                </StackPanel>
                <StackPanel Orientation="Horizontal" HorizontalAlignment="Center">
                    <Button x:Name="elerheto_btn" Content="El�rhet�" Margin="10 10 10 10" Background="Green" Foreground="White" FontSize="18"/>
                    <Button x:Name="tulterhelt_btn" Content="T�lterhelt" Margin="10 10 10 10" Background="Gold" Foreground="White" FontSize="18"/>
                    <Button x:Name="beteg_btn" Content="Beteg" Margin="10 10 10 10" Background="Red" Foreground="White" FontSize="18"/>
                </StackPanel>
            </StackPanel>

        </StackPanel>
    </Border>
</Window>
