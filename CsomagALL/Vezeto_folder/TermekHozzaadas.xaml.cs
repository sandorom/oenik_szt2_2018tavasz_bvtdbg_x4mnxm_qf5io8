﻿// <copyright file="TermekHozzaadas.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Vezeto_folder
{
    using System;
    using System.Windows;
    using Data;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for TermekHozzaadas.xaml
    /// </summary>
    public partial class TermekHozzaadas : Window
    {
        /// <summary>
        /// Raktárvezető referencia
        /// </summary>
        private RaktarVezetoVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="TermekHozzaadas"/> class.
        /// </summary>
        /// <param name="vm_">Raktárvezető referencia</param>
        public TermekHozzaadas(RaktarVezetoVM vm_)
        {
            this.InitializeComponent();
            this.vm = vm_;
            this.ablakNev.Content = "Új termék";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TermekHozzaadas"/> class.
        /// </summary>
        /// <param name="vm_">Raktárvezető referencia</param>
        /// <param name="termek">Módosítani kívánt termék</param>
        public TermekHozzaadas(RaktarVezetoVM vm_, TERMEK termek)
        {
            this.InitializeComponent();
            this.vm = vm_;
            this.ablakNev.Content = "Termék módosítása";
            this.ar_tb.Text = termek.Ar.Value.ToString();
            this.nev_tb.Text = termek.Nev;
            this.suly_tb.Text = termek.Suly.Value.ToString();
            this.leiras_tb.Text = termek.Leiras;
        }

        private void Rogzit_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TERMEK termek = new TERMEK();
                termek.Ar = decimal.Parse(this.ar_tb.Text);
                termek.Nev = this.nev_tb.Text;
                termek.Suly = decimal.Parse(this.suly_tb.Text);
                termek.Leiras = this.leiras_tb.Text;
                int mennyiseg = int.Parse(this.mennyiseg_tb.Text);
                if (this.ablakNev.Content.Equals("Új termék"))
                {
                    termek.ID = (int)(Math.Abs(termek.Nev.GetHashCode()) - termek.Suly);
                    this.vm.UjTermekMentes(termek);
                }
                else
                {
                    this.vm.TermekModositas(termek);
                }

                this.vm.KeszletNoveles(termek, mennyiseg);
                MessageBox.Show("Termék rögzítve!", "Sikeres rögzítés!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message, "Nem sikerült a rögzítés!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (FormatException)
            {
                MessageBox.Show("Kérjük ellenőrizze a bevitt adatokat!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
