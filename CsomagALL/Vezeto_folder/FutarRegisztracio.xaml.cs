﻿// <copyright file="FutarRegisztracio.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Vezeto_folder
{
    using System.Threading.Tasks;
    using System.Windows;
    using Data;
    using Logic.Authentication;
    using Logic.Common;
    using Logic.Enums;

    /// <summary>
    /// Interaction logic for FutarRegisztracio.xaml
    /// </summary>
    public partial class FutarRegisztracio : Window
    {
        /// <summary>
        /// Raktárvezető referencia
        /// </summary>
        private RAKTARVEZETO rv;

        /// <summary>
        /// Regisztrálás folyamatok
        /// </summary>
        private RegisztracioService regisztracioService = new RegisztracioService();

        /// <summary>
        /// Initializes a new instance of the <see cref="FutarRegisztracio"/> class.
        /// </summary>
        /// <param name="rv">Raktárvezető referencia</param>
        public FutarRegisztracio(RAKTARVEZETO rv)
        {
            this.InitializeComponent();
            this.rv = rv;
        }

        /// <summary>
        /// Regisztráció
        /// </summary>
        /// <param name="sender">Küldő objektum</param>
        /// <param name="e">Esemény paraméterei</param>
        private void Rogzit_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string username = this.username_tb.Text;
                string jelszo = this.password_tb.Password;
                string teljesNev = this.teljesNev_tb.Text;
                string korzet = this.korzet_tb.Text;
                string rendszam = this.rendszam_tb.Text;
                try
                {
                    if ((string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(jelszo) || string.IsNullOrWhiteSpace(teljesNev)
                    || string.IsNullOrWhiteSpace(korzet) || string.IsNullOrWhiteSpace(rendszam)) && this.RendszamValidalas(rendszam))
                    {
                        MessageBox.Show("Minden mezőt töltsön ki!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        this.regisztracioService.Regisztralas(teljesNev, username, jelszo, JogosultsagEnum.Futar.ToString(), string.Empty, string.Empty, string.Empty, rendszam, decimal.Parse(korzet));
                        Task.Run(() => Logger.UJFelhasznaloRogzitesLog(this.rv, teljesNev, username, string.Empty, string.Empty, string.Empty, rendszam));
                        MessageBox.Show("Jelentkezzen be!", "Sikeres regisztráció!", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
                catch (CsomagAllException ex)
                {
                    MessageBox.Show(ex.Message, "Sikertelen regisztráció!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (CsomagAllException ex)
            {
                MessageBox.Show(ex.Message, "Sikertelen regisztráció!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        /// <summary>
        /// Ellenőrzi, hogy a rendszámot megfelelő formátumban adták-e meg
        /// </summary>
        /// <param name="rendszam">Rendszám</param>
        /// <returns>Valid rendszám</returns>
        private bool RendszamValidalas(string rendszam)
        {
            if (rendszam.Length == 6 && !rendszam.Contains("-"))
            {
                return true;
            }

            MessageBox.Show("A rendszámot a következő formátumban adja meg: ABC123.", "Helytelen rendszám formátum!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            return false;
        }
    }
}
