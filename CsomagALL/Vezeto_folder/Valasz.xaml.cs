﻿// <copyright file="Valasz.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Vezeto_folder
{
    using System.Windows;
    using Data;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for Valasz.xaml
    /// </summary>
    public partial class Valasz : Window
    {
        /// <summary>
        /// Megrendelő ViewModel referencia
        /// </summary>
        private RaktarVezetoVM vm;

        /// <summary>
        /// Az az üzenet, amire a felhasználó válaszolni szeretne
        /// </summary>
        private UZENET elozmenyUzenet;

        /// <summary>
        /// Initializes a new instance of the <see cref="Valasz"/> class.
        /// </summary>
        /// <param name="vm_">Raktárvezető viewmodel referencia</param>
        /// <param name="elozmenyUzenet">Annak az üzenetnek a referenciája, amire válaszol a vezető</param>
        public Valasz(RaktarVezetoVM vm_, UZENET elozmenyUzenet)
        {
            this.InitializeComponent();
            this.vm = vm_;
            string cimzettNev = this.vm.CimzettNevFeloldas(elozmenyUzenet.Kuldo_ID);
            this.cimzett_tb.Text = cimzettNev;
            this.elozmenyUzenet = elozmenyUzenet;
            this.targy_tb.Text = "Re: " + elozmenyUzenet.Targy;
        }

        /// <summary>
        /// Küldés gomb logikája.
        /// </summary>
        /// <param name="sender">Eseményt küldő objektum</param>
        /// <param name="e">Esemény argumentumai</param>
        private void Kuld_btn_Click(object sender, RoutedEventArgs e)
        {
            this.vm.UJUzenetParameterezesESKuldes(decimal.ToInt32(this.elozmenyUzenet.Kuldo_ID), this.targy_tb.Text, this.szoveg_tb.Text);
            MessageBox.Show("Üzenet elküldve!", "Sikeres üzenet küldés!", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }
    }
}
