﻿// <copyright file="Vezeto.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Vezeto_folder
{
    using System.Collections.Generic;
    using System.Windows;
    using Data;
    using Data.Common;
    using Logic.Common;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for Vezeto.xaml
    /// </summary>
    public partial class Vezeto : Window
    {
        private List<SZALLITAS> rendelesek;
        private List<FUTAR> futarok;
        private FUTAR valasztottFutar;
        private SZALLITAS valasztottRendeles;

        public Vezeto(RAKTARVEZETO rv)
        {
            this.InitializeComponent();
            try
            {
                this.VM = new RaktarVezetoVM(rv);
            }
            catch (CsomagAllException ex)
            {
                MessageBox.Show(ex.Message, "Sikertelen bejelentkezés!", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            this.DataContext = this;
            this.osszesTermek_lb.ItemsSource = this.VM.ElerhetoTermekekRaktarSzerint(this.VM.Raktar);
            this.futarok = this.VM.FutarokRaktarSzerint();
            this.rendelesek = this.VM.Szallitasok();
            this.uzenetek_lb.ItemsSource = this.VM.FelhasznaloUzenetei(rv.ID);
        }

        /// <summary>
        /// Gets raktárvezető viewmodel
        /// </summary>
        private RaktarVezetoVM VM { get; }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            // this.rendelesek_sp.Visibility = Visibility.Collapsed;
            this.futarok_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;
            this.termekek_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.termekek_sp.Visibility = Visibility.Collapsed;
            this.futarok_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;

            // this.rendelesek_sp.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            // this.rendelesek_sp.Visibility = Visibility.Collapsed;
            this.termekek_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;
            this.futarok_sp.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Új futár felvétele
        /// </summary>
        /// <param name="sender">Küldő objektum</param>
        /// <param name="e">Esemény paraméterei</param>
        private void Futar_reg_btn_Click(object sender, RoutedEventArgs e)
        {
            FutarRegisztracio fr = new FutarRegisztracio(this.VM.RaktarVezeto);
            fr.ShowDialog();
        }

        private void Torles_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.osszesTermek_lb.SelectedItem != null)
            {
                this.VM.TermekTorlese(this.osszesTermek_lb.SelectedItem as TERMEK);
            }
        }

        private void Szerk_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.osszesTermek_lb.SelectedItem != null)
            {
                TermekHozzaadas th = new TermekHozzaadas(this.VM, this.osszesTermek_lb.SelectedItem as TERMEK);
                th.ShowDialog();
            }
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            // this.rendelesek_sp.Visibility = Visibility.Collapsed;
            this.termekek_sp.Visibility = Visibility.Collapsed;
            this.futarok_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Visible;
        }

        private void Hozzarendel_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.valasztottFutar != null && this.valasztottRendeles != null)
            {
                this.VM.SzallitasFutarOsszerendeles(this.valasztottRendeles as SZALLITAS, this.valasztottFutar as FUTAR);
                MessageBox.Show(this.valasztottRendeles.ID + " rendelés hozzárendelve a következő futárhoz: " + this.valasztottFutar.Nev, "Sikeres hozzárendelés!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Jelöljön ki egy futárt és egy rendelést is!");
            }
        }

        private void Valasz_btn_Click(object sender, RoutedEventArgs e)
        {
            UzenetForGui kivalasztottUzenet = (UzenetForGui)this.uzenetek_lb.SelectedItem;
            if (kivalasztottUzenet != null)
            {
                Valasz v = new Valasz(this.VM, kivalasztottUzenet.Uzenet);
                v.Show();
            }
        }

        private void Uj_btn_Click(object sender, RoutedEventArgs e)
        {
            TermekHozzaadas th = new TermekHozzaadas(this.VM);
            th.Show();
        }

        // GETTERS & SETTERS

        public List<SZALLITAS> Rendelesek
        {
            get
            {
                return this.rendelesek;
            }

            set
            {
                this.rendelesek = value;
            }
        }

        public List<FUTAR> Futarok
        {
            get
            {
                return this.futarok;
            }

            set
            {
                this.futarok = value;
            }
        }

        public FUTAR ValasztottFutar
        {
            get
            {
                return this.valasztottFutar;
            }

            set
            {
                this.valasztottFutar = value;
            }
        }

        public SZALLITAS ValasztottRendeles
        {
            get
            {
                return this.valasztottRendeles;
            }

            set
            {
                this.valasztottRendeles = value;
            }
        }
    }
}
