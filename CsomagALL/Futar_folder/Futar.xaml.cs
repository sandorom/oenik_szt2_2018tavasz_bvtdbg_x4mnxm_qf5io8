﻿// <copyright file="Futar.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Futar_folder
{
    using System.Windows;
    using Data;
    using Logic.Enums;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for Futar.xaml
    /// </summary>
    public partial class Futar : Window
    {
        public Futar(FUTAR futar)
        {
            this.InitializeComponent();
            this.VM = new FutarVM(futar);
            this.allapot_sp.DataContext = this.VM.Futar;
            if (this.VM.Szallitasok.Count > 0)
            {
                this.csomagok_sp.DataContext = this.VM.Szallitasok[0];
            }
        }

        /// <summary>
        /// Gets futár viewmodel
        /// </summary>
        public FutarVM VM { get; }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Csomagok_Click(object sender, RoutedEventArgs e)
        {
            this.allapot_sp.Visibility = Visibility.Collapsed;
            this.csomagok_sp.Visibility = Visibility.Visible;
        }

        private void Allapot_Click(object sender, RoutedEventArgs e)
        {
            this.csomagok_sp.Visibility = Visibility.Collapsed;
            this.allapot_sp.Visibility = Visibility.Visible;
        }

        private void Elerheto_btn_Click(object sender, RoutedEventArgs e)
        {
            this.VM.Futar.Elerhetoseg = FutarAllapotEnum.Elerheto.ToString();
            this.VM.ElerhetosegFrissites(this.VM.Futar);
            this.allapotadatkotve_label.Content = this.elerheto_btn.Content;
        }

        private void Tulterhelt_btn_Click(object sender, RoutedEventArgs e)
        {
            this.VM.Futar.Elerhetoseg = FutarAllapotEnum.Tulterhelt.ToString();
            this.VM.ElerhetosegFrissites(this.VM.Futar);
            this.allapotadatkotve_label.Content = this.tulterhelt_btn.Content;
        }

        private void Beteg_btn_Click(object sender, RoutedEventArgs e)
        {
            this.VM.Futar.Elerhetoseg = FutarAllapotEnum.Beteg.ToString();
            this.VM.ElerhetosegFrissites(this.VM.Futar);
            this.allapotadatkotve_label.Content = this.beteg_btn.Content;
        }

        private void Sikeres_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.VM.Szallitasok.Count > 0)
            {
                this.VM.Szallitasok[0].Allapot = SzallitasAllapotEnum.Kiszallitva.ToString();
                this.VM.AllapotFrissites(this.VM.Szallitasok[0]);
                this.VM.SzallitasFrissites();
                if (this.VM.Szallitasok.Count > 0)
                {
                    this.csomagok_sp.DataContext = this.VM.Szallitasok[0];
                }
                else
                {
                    this.csomagok_sp.DataContext = null;
                }

                MessageBox.Show("Változás rögzítve!");
            }
        }

        private void Sikertelen_btn_Click(object sender, RoutedEventArgs e)
        {
            if (this.VM.Szallitasok.Count > 0)
            {
                this.VM.Szallitasok[0].Allapot = SzallitasAllapotEnum.SikertelenKezbesites.ToString();
                this.VM.AllapotFrissites(this.VM.Szallitasok[0]);
                this.VM.SzallitasFrissites();
                if (this.VM.Szallitasok.Count > 0)
                {
                    this.csomagok_sp.DataContext = this.VM.Szallitasok[0];
                }
                else
                {
                    this.csomagok_sp.DataContext = null;
                }

                MessageBox.Show("Változás rögzítve!");
            }
        }
    }
}
