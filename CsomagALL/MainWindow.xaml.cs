﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL
{
    using System.Threading.Tasks;
    using System.Windows;
    using Data;
    using Data.Authentication;
    using Futar_folder;
    using Logic.Authentication;
    using Logic.Common;
    using Logic.Enums;
    using Megrendelo_folder;
    using Vezeto_folder;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();

            // az első bejelentkezés nagyon lassú, ezért van ez a dirty-hack
            LoginDAO lDAO = new LoginDAO();
            Task init = Task.Run(() => lDAO.PreLoad());

            // logoláshoz szükséges fájlok inicializálása
            init.ContinueWith(t => Logger.Init(), TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        /// <summary>
        /// Bejelentkezés folyamatának elindítása
        /// </summary>
        /// <param name="sender">Küldő objektum (Register gomb)</param>
        /// <param name="e">Esemény argumentumai</param>
        private void Login(object sender, RoutedEventArgs e)
        {
            try
            {
                object[] eredmeny = LoginService.Bejelentkezes(this.username_tb.Text, this.password_tb.Password);
                string jog = (string)eredmeny[0];
                if (string.Compare(JogosultsagEnum.Megrendelo.ToString(), jog, true) == 0)
                {
                    MEGRENDELO user = eredmeny[1] as MEGRENDELO;
                    Megrendelo megr = new Megrendelo(user);
                    Task.Run(() => Logger.LoginLog(user.Nev, user.ID, JogosultsagEnum.Megrendelo));
                    this.Hide();
                    megr.ShowDialog();
                    this.Show();
                }
                else if (string.Compare(JogosultsagEnum.Vezeto.ToString(), jog, true) == 0)
                {
                    RAKTARVEZETO user = eredmeny[1] as RAKTARVEZETO;
                    Vezeto vez = new Vezeto(user);
                    Task.Run(() => Logger.LoginLog(user.Nev, user.ID, JogosultsagEnum.Vezeto));
                    this.Hide();
                    vez.ShowDialog();
                    this.Show();
                }
                else if (string.Compare(JogosultsagEnum.Futar.ToString(), jog, true) == 0)
                {
                    FUTAR user = eredmeny[1] as FUTAR;
                    Futar fut = new Futar(user);
                    Task.Run(() => Logger.LoginLog(user.Nev, user.ID, JogosultsagEnum.Futar));
                    this.Hide();
                    fut.ShowDialog();
                    this.Show();
                }
            }
            catch (CsomagAllException ex)
            {
                MessageBox.Show(ex.Message, "Sikertelen bejelentkezés!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// Regisztrálás folyamatának elindítása
        /// </summary>
        /// <param name="sender">Küldő objektum (Register gomb)</param>
        /// <param name="e">Esemény argumentumai</param>
        private void Register(object sender, RoutedEventArgs e)
        {
            MegrendeloRegisztracio reg = new MegrendeloRegisztracio();
            reg.ShowDialog();
        }
    }
}
