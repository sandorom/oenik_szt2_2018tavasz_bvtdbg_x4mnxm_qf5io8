﻿// <copyright file="Valasz.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Megrendelo_folder
{
    using System.Windows;
    using Data;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for Valasz.xaml
    /// </summary>
    public partial class Valasz : Window
    {
        /// <summary>
        /// Megrendelő ViewModel referencia
        /// </summary>
        private MegrendeloVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="Valasz"/> class.
        /// </summary>
        /// <param name="vm_">Megrendelő viewmodel referencia</param>
        public Valasz(MegrendeloVM vm_)
        {
            this.InitializeComponent();
            this.vm = vm_;
            this.cimzett_cb.ItemsSource = this.vm.RaktarVezetok();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Valasz"/> class.
        /// </summary>
        /// <param name="vm_">Megrendelő viewmodel referencia</param>
        /// <param name="elozmenyUzenet">Annak az üzenetnek a referenciája, amire válaszol a megrendelő</param>
        public Valasz(MegrendeloVM vm_, UZENET elozmenyUzenet)
        {
            this.InitializeComponent();
            this.vm = vm_;
            this.cimzett_cb.ItemsSource = this.vm.RaktarVezetok();
            foreach (RAKTARVEZETO item in this.vm.RaktarVezetok())
            {
                if (item.ID == elozmenyUzenet.Kuldo_ID || item.ID == elozmenyUzenet.Cimzett_ID)
                {
                    this.cimzett_cb.SelectedItem = item;
                    this.targy_tb.Text = "Re: " + elozmenyUzenet.Targy;
                }
            }
        }

        /// <summary>
        /// Küldés gomb logikája.
        /// </summary>
        /// <param name="sender">Eseményt küldő objektum</param>
        /// <param name="e">Esemény argumentumai</param>
        private void Kuld_btn_Click(object sender, RoutedEventArgs e)
        {
            int cimzettID = decimal.ToInt32(((RAKTARVEZETO)this.cimzett_cb.SelectedItem).ID);
            this.vm.UJUzenetParameterezesESKuldes(cimzettID, this.targy_tb.Text, this.szoveg_tb.Text);
            MessageBox.Show("Üzenet elküldve!", "Sikeres üzenet küldés!", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Close();
        }
    }
}
