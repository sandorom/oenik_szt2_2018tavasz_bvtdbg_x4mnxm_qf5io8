﻿// <copyright file="MegrendeloRegisztracio.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Megrendelo_folder
{
    using System.Threading.Tasks;
    using System.Windows;
    using Logic.Common;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for MegrendeloRegisztracio.xaml
    /// </summary>
    public partial class MegrendeloRegisztracio : Window
    {
        /// <summary>
        /// Megrendelő viewmodel
        /// </summary>
        private MegrendeloVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="MegrendeloRegisztracio"/> class.
        /// </summary>
        public MegrendeloRegisztracio()
        {
            this.InitializeComponent();
            this.vm = new MegrendeloVM(null); // mivel csak regisztráció lesz, nem gond, ha null a bejelentkezett user
        }

        /// <summary>
        /// Regisztráció
        /// </summary>
        /// <param name="sender">Küldő objektum</param>
        /// <param name="e">Esemény paraméterei</param>
        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            string username = this.username_tb.Text;
            string jelszo = this.password_tb.Password;
            string iranyitoszam = this.iranitoszam_tb.Text;
            string varos = this.varos_tb.Text;
            string cim = this.cim_tb.Text;
            string telszam = this.tel_tb.Text;
            string teljesNev = this.teljesNev_tb.Text;
            try
            {
                if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(jelszo) || string.IsNullOrWhiteSpace(teljesNev)
                || string.IsNullOrWhiteSpace(iranyitoszam) || string.IsNullOrWhiteSpace(telszam) || string.IsNullOrWhiteSpace(cim)
                || string.IsNullOrWhiteSpace(varos))
                {
                    MessageBox.Show("Minden mezőt töltsön ki!", "Hiba!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    this.vm.Regisztralas(teljesNev, username, jelszo, iranyitoszam, varos, cim);
                    Task.Run(() => Logger.UJFelhasznaloRogzitesLog(null, teljesNev, username, iranyitoszam, varos, cim, string.Empty));
                    MessageBox.Show("Jelentkezzen be!", "Sikeres regisztráció!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (CsomagAllException ex)
            {
                MessageBox.Show(ex.Message, "Sikertelen regisztráció!", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
