﻿// <copyright file="Megrendelo.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace CsomagALL.Megrendelo_folder
{
    using System.Windows;
    using Data;
    using Data.Common;
    using Logic.Common;
    using Logic.ViewModels;

    /// <summary>
    /// Interaction logic for Megrendelo.xaml
    /// </summary>
    public partial class Megrendelo : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Megrendelo"/> class.
        /// </summary>
        /// <param name="megrendelo">Megrendelő viewmodel</param>
        public Megrendelo(MEGRENDELO megrendelo)
        {
            this.InitializeComponent();
            this.VM = new MegrendeloVM(megrendelo);
            this.osszesTermek_lb.ItemsSource = this.VM.ElerhetoTermekek();
            this.kosarTermek_lb.ItemsSource = this.VM.Kosar;
            this.uzenetek_lb.ItemsSource = this.VM.FelhasznaloUzenetei(megrendelo.ID);
            this.rendeles_lb.ItemsSource = this.VM.Rendelesek();
        }

        /// <summary>
        /// Gets megrendelő viewmodel
        /// </summary>
        public MegrendeloVM VM { get; }

        private void Termekek_Click(object sender, RoutedEventArgs e)
        {
            this.kosar_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;
            this.rendeles_sp.Visibility = Visibility.Collapsed;

            this.termekek_sp.Visibility = Visibility.Visible;
        }

        private void Kosar_Click(object sender, RoutedEventArgs e)
        {
            this.termekek_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;
            this.rendeles_sp.Visibility = Visibility.Collapsed;

            this.kosar_sp.Visibility = Visibility.Visible;
            this.kosarTermek_lb.ItemsSource = this.VM.Kosar;
        }

        private void Uzenetek_Click(object sender, RoutedEventArgs e)
        {
            this.kosar_sp.Visibility = Visibility.Collapsed;
            this.termekek_sp.Visibility = Visibility.Collapsed;
            this.rendeles_sp.Visibility = Visibility.Collapsed;

            this.uzenet_sp.Visibility = Visibility.Visible;
        }

        private void Rendelesek_Click(object sender, RoutedEventArgs e)
        {
            this.kosar_sp.Visibility = Visibility.Collapsed;
            this.uzenet_sp.Visibility = Visibility.Collapsed;
            this.termekek_sp.Visibility = Visibility.Collapsed;

            this.rendeles_sp.Visibility = Visibility.Visible;
            this.rendeles_lb.ItemsSource = this.VM.Rendelesek();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Kosarba_btn_Click(object sender, RoutedEventArgs e)
        {
            this.VM.KosarbaHelyez((TERMEK)this.osszesTermek_lb.SelectedItem, int.Parse(this.termekMennyiseg_tb_termekek.Text), 1);
            this.kosarTermek_lb.ItemsSource = this.VM.Kosar;
            this.teljesAr_tb_kosar.Text = this.VM.VegOsszeg.ToString();
        }

        private void Rendel_btn_kosar_Click(object sender, RoutedEventArgs e)
        {
            string cim = "Sikeres rendelés!";
            string tartalom = "Rendelése rögzítésre került!";
            try
            {
                this.VM.Megrendel();
            }
            catch (CsomagAllException ex)
            {
                cim = "Sikertelen rendelés!";
                tartalom = ex.Message;
            }

            MessageBox.Show(tartalom, cim, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Valasz_btn_Click(object sender, RoutedEventArgs e)
        {
            UzenetForGui kivalasztottUzenet = (UzenetForGui)this.uzenetek_lb.SelectedItem;
            if (kivalasztottUzenet != null)
            {
                Valasz v = new Valasz(this.VM, kivalasztottUzenet.Uzenet);
                v.Show();
            }
        }

        private void Ujuzenet_btn_Click(object sender, RoutedEventArgs e)
        {
            Valasz v = new Valasz(this.VM);
            v.Show();
        }

        private void Torles_btn_rendeles_Click(object sender, RoutedEventArgs e)
        {
            SZALLITAS valasztottSzallitas = (SZALLITAS)this.rendeles_lb.SelectedItem;
            if (valasztottSzallitas != null)
            {
                try
                {
                    this.VM.RendelesTorlese(valasztottSzallitas);
                    MessageBox.Show("Rendelés törölve!", "Sikeres törlés!", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (CsomagAllException ex)
                {
                    MessageBox.Show(ex.Message, "Sikertelen törlés!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
    }
}
